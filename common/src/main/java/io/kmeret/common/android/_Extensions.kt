package io.kmeret.common.android

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatSeekBar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar

fun GestureDetectorCompat.onDoubleTap(action: () -> Unit) {
    setOnDoubleTapListener(object : GestureDetector.OnDoubleTapListener {
        override fun onDoubleTap(e: MotionEvent?): Boolean {
            action.invoke()
            return true
        }

        override fun onDoubleTapEvent(e: MotionEvent?) = false
        override fun onSingleTapConfirmed(e: MotionEvent?) = false
    })
}

fun View.visibility(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun AppCompatSeekBar.onProgressChangedFromUser(body: (progress: Int) -> Unit) {
    setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekArc: SeekBar, progress: Int, fromUser: Boolean) {
            if (fromUser) body(progress)
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {}
        override fun onStopTrackingTouch(p0: SeekBar?) {}
    })
}

fun SeekBar.onProgressChanged(body: (progress: Int) -> Unit) {
    setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekArc: SeekBar, progress: Int, fromUser: Boolean) {
            body(progress)
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {}
        override fun onStopTrackingTouch(p0: SeekBar?) {}
    })
}

fun ViewGroup.inflate(layoutRes: Int, attach: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attach)
}

fun Context.fromDp(dp: Int) =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), this.resources.displayMetrics)

fun Context.resolveFragmentManager(): FragmentManager {
    return when (this) {
        is AppCompatActivity -> supportFragmentManager
        is Fragment -> childFragmentManager
        else -> throw Exception("Cannot resolve!")
    }
}

@Suppress("DEPRECATION")
fun View.keyboardVisible(visible: Boolean) {
    val service = (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
    if (visible)
        service.showSoftInputFromInputMethod(windowToken, InputMethodManager.SHOW_FORCED)
    else
        service.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

fun View.onClick(callback: () -> Unit) {
    setOnClickListener { callback.invoke() }
}

fun View.onLongClick(action: () -> Unit) {
    setOnLongClickListener {
        action.invoke()
        return@setOnLongClickListener true
    }
}

fun View.onFocus(callback: (hasFocus: Boolean) -> Unit) {
    setOnFocusChangeListener { _, hasFocus -> callback.invoke(hasFocus) }
}

fun RecyclerView.attachAdapter(adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder>, reverse: Boolean = false) {
    setHasFixedSize(false)
    layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, reverse)
    this.adapter = adapter
}

fun RecyclerView.attachGridAdapter(adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder>, columns: Int = 2) {
    setHasFixedSize(false)
    layoutManager = GridLayoutManager(context, columns)
    this.adapter = adapter
}

fun ImageView.vectorColor(res: Int) = setColorFilter(ContextCompat.getColor(context, res))

fun ImageView.setImageVector(resId: Int) {
    setImageDrawable(ContextCompat.getDrawable(context, resId))
}

fun EditText.onTextChanged(listener: (text: String, length: Int) -> Unit) {
    val editText = this
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s == null) return
            val fromUser = Math.abs(count - before) == 1
            if (fromUser) listener.invoke(s.toString(), editText.text.length)
        }
    })
}

fun requestConfirmDialog(context: Context, onSuccess: () -> Unit) {
    AlertDialog.Builder(context)
            .setTitle("Delete this file?")
            .setPositiveButton("Delete") { _, _ -> onSuccess.invoke() }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .create().show()
}

fun String.toEditable() = SpannableStringBuilder(this)

@Suppress("DEPRECATION")
@SuppressLint("MissingPermission")
fun Context.isOnline(): Boolean {
    val manager = (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
    val network = manager.activeNetworkInfo
    return (network != null && network.isAvailable && network.isConnected)
}

fun Int.padStart(length: Int, padChar: Char) : String{
    return this.toString().padStart(length, padChar)
}