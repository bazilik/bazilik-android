package io.kmeret.common.android

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment() {

    abstract val layoutResId: Int

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View =
            layoutInflater.inflate(layoutResId, container, false)

    protected fun showBackArrow(action: (() -> Unit)? = null) {
        (activity as BaseActivity).apply {
            backArrowVisible(true)
            onBackPressed = action
        }
    }

    protected fun hideBackArrow() {
        (activity as BaseActivity).apply {
            backArrowVisible(false)
            onBackPressed = null
        }
    }

    protected fun changeFragment(fragment: Fragment, slideAnimation: Boolean = true) {
        (activity as BaseActivity).changeFragment(fragment, slideAnimation)
    }

    protected fun closeFragment() {
        requireActivity().onBackPressed()
    }

    protected fun changeTitle(title: String) {
        (activity as BaseActivity).changeTitle(title)
    }

    protected fun getPermissionManager() = (activity as BaseActivity).getPermissionManager()

}