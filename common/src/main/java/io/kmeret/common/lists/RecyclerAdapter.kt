package io.kmeret.common.lists

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class RecyclerAdapter<T : Identifiable>(private val templateResId: Int,
                                        private val onBindItem: ((item: T, rootView: View) -> Unit))

    : RecyclerView.Adapter<RecyclerAdapter<T>.ItemViewHolder>() {

    inner class ItemViewHolder(val rootView: View) : RecyclerView.ViewHolder(rootView)

    private var list: List<T> = emptyList()

    init {
        this.setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(templateResId, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) =
            onBindItem.invoke(list[position], holder.rootView)

    override fun getItemCount() = list.size

    override fun getItemId(position: Int) = list[position].id

    fun updateList(list: List<T>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun getList() = list

}