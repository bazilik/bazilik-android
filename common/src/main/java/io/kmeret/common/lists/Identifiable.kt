package io.kmeret.common.lists

interface Identifiable {
    val id: Long
}