package io.kmeret.bazilik.message

import io.kmeret.common.lists.Identifiable

data class DishMessageDto(
        override val id: Long = 0,
        val comment: String,
        var sentAt: Long = 0,

        val userFromId: Long,
        val userFromName: String,
        val userFromAvatarUrl: String,

        val shopId: Long,
        val shopName: String,
        val shopLogoUrl: String,

        val dishId: Long,
        val dishName: String,
        val dishPhotoUrl: String,
        val dishCommentCount: Int
) : Identifiable