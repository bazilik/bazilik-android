package io.kmeret.bazilik.message

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.frame.toDateTimeString
import io.kmeret.bazilik.share.ShareDialog
import io.kmeret.bazilik.shop.cart.add.AddCartDialog
import io.kmeret.bazilik.shop.dish.DishFragment
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_chat.*
import kotlinx.android.synthetic.main.template_message.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class MessageListFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_chat
    private val viewModel by viewModel<MessageListViewModel>()
    private lateinit var messageListAdapter: RecyclerAdapter<DishMessageDto>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.recommendations))

        initMessageListView()

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.messageList.subscribe(this) { messageListAdapter.updateList(it) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.viewMessageList(messageListAdapter.getList().map { it.id })
    }

    private fun initMessageListView() {
        messageListAdapter = RecyclerAdapter(R.layout.template_message) { item, rootView ->
            with(rootView) {
                userFromAvatarView.loadWithUrl(item.userFromAvatarUrl)
                shopLogoView.loadWithUrl(item.shopLogoUrl)
                dishPhotoView.loadWithUrl(item.dishPhotoUrl)

                userFromNameView.text = item.userFromName
                messageSentAtView.text = item.sentAt.toDateTimeString()
                messageCommentView.text = item.comment
                dishNameView.text = item.dishName
                commentCountView.text = item.dishCommentCount.toString()

                addCartButton.onClick { openAddCartModal(item.dishId) }
                shareButton.onClick { openShareModal(item.dishId) }
                commentButton.onClick { changeFragment(DishFragment.create(item.dishId), false) }
                dishPhotoView.onClick { changeFragment(DishFragment.create(item.dishId), false) }

            }
        }
        messageListView.attachAdapter(messageListAdapter)
    }

    private fun openAddCartModal(dishId: Long) =
            AddCartDialog.create(dishId).also {
                it.show(childFragmentManager, AddCartDialog.TAG)
            }

    private fun openShareModal(dishId: Long) =
            ShareDialog.create(dishId).also {
                it.show(childFragmentManager, ShareDialog.TAG)
            }

}