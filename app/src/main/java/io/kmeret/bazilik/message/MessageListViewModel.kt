package io.kmeret.bazilik.message

import io.kmeret.bazilik.frame.BaseViewModel

class MessageListViewModel(private val messageListInteractor: MessageListInteractor)
    : BaseViewModel(listOf(messageListInteractor)) {

    val messageList = messageListInteractor.getMessageList()

    fun viewMessageList(idList: List<Long>) =
            messageListInteractor.postViewedMessageList(idList)

}