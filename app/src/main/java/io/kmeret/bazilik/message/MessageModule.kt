package io.kmeret.bazilik.message

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object MessageModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { MessageListInteractor(get(), get(), get(), get(), get(), get(), get(), get()) }
            viewModel { MessageListViewModel(get()) }
        }.invoke()
    }
}