package io.kmeret.bazilik.message

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiViewedMessageList
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.MessageNotViewedException
import io.kmeret.bazilik.storage.dao.*

class MessageListInteractor(private val apiService: ApiService,
                            private val authUserDao: AuthUserDao,
                            private val tokenManager: TokenManager,
                            private val messageDao: MessageDao,
                            private val shopDao: ShopDao,
                            private val categoryDao: CategoryDao,
                            private val dishDao: DishDao,
                            private val userDao: UserDao
) : BaseInteractor() {

    fun getUnviewedMessagesCount() = messageDao.getUnviewedMessagesCount()

    fun getMessageList() = messageDao.getMessageList()

    fun requestUnviewedMessageList() {
        val authUser = authUserDao.getCurrent() ?: return

        apiService.getUnviewedMessageList(tokenManager.getAccessToken(), authUser.id).request {
            if (it.isEmpty()) return@request

            it.forEach {
                shopDao.save(it.dish.category.shop.map())
                categoryDao.save(it.dish.category.map())
                dishDao.save(it.dish.map())
                userDao.save(it.userFrom.mapUser())
                messageDao.save(it.map())
            }
        }
    }

    fun postViewedMessageList(list: List<Long>) {
        if (list.isEmpty()) return

        apiService.sendViewedMessageIdList(tokenManager.getAccessToken(), ApiViewedMessageList(list))
                .request {
                    if (!it.success) {
                        onErrorCallback?.invoke(MessageNotViewedException())
                        return@request
                    }

                    messageDao.viewMessageList(list)
                }
    }
}