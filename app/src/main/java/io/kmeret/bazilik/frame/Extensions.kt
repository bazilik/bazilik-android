package io.kmeret.bazilik.frame

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import io.kmeret.common.android.padStart
import java.util.*

fun <T> LiveData<T>.subscribe(owner: LifecycleOwner, callback: (T) -> Unit) =
        observe(owner, Observer { if (it == null) return@Observer; callback.invoke(it) })

fun Long.toDateTimeString() = with(Calendar.getInstance()) {
    this.timeInMillis = this@toDateTimeString
    "${get(Calendar.DAY_OF_MONTH)}." +
            "${(get(Calendar.MONTH) + 1).padStart(2, '0')} " +
            "${get(Calendar.HOUR_OF_DAY)}:" +
            get(Calendar.MINUTE).padStart(2, '0')
}