package io.kmeret.bazilik.frame

import android.arch.lifecycle.ViewModel
import io.kmeret.bazilik.R

abstract class BaseViewModel(interactors: List<BaseInteractor>) : ViewModel() {

    val errorMessage = SingleLiveEvent<Int>()

    init {
        interactors.forEach {
            it.onErrorCallback = { ex ->
                errorMessage.value = when (ex) {
                    is EmptyListException -> R.string.exceptionEmptyList
                //
                    is UserNotAuthException -> R.string.exceptionUserNotAuth
                    is FirebaseAuthFailedException -> R.string.exceptionFirebaseAuthFailed
                    is VkException -> R.string.exceptionVk
                    is ApiException -> R.string.exceptionApi
                //
                    is AddressNotDeletedException -> R.string.exceptionAddressNotDeleted
                    is ContactsNotImportedException -> R.string.exceptionContactsNotImported
                    is OrderNotSentException -> R.string.exceptionOrderNotSent
                    is FeedbackNotSentException -> R.string.exceptionFeedbackNotSent
                    is MessageNotSentException -> R.string.exceptionMessageNotSent
                    else -> R.string.exceptionUnknown
                }
                ex.printStackTrace()
            }
        }
    }

}
