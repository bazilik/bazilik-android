package io.kmeret.bazilik.frame

class EmptyListException : Exception()

class UserNotAuthException : Exception()

class FirebaseAuthFailedException : Exception()

class VkException : Exception()

class ApiException : Exception()

class AddressNotDeletedException : Exception()

class ContactsNotImportedException : Exception()

class OrderNotSentException : Exception()

class FeedbackNotSentException : Exception()

class MessageNotViewedException : Exception()

class MessageNotSentException : Exception()
