package io.kmeret.bazilik.frame

import io.kmeret.bazilik.api.makeRequest
import retrofit2.Call

abstract class BaseInteractor {

    var onErrorCallback: ((Exception) -> Unit)? = null

    fun <T> Call<T>.request(onSuccess: (response: T) -> Unit) {
        makeRequest(onSuccess, onError = { onErrorCallback?.invoke(it) })
    }

}