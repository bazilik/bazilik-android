package io.kmeret.bazilik.feedback

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.frame.SingleLiveEvent

class FeedbackViewModel(private val feedbackInteractor: FeedbackInteractor,
                        userInteractor: UserInteractor
) : BaseViewModel(listOf(feedbackInteractor, userInteractor)) {

    private val currentUser = userInteractor.getAuthorized()
    val feedbackAvailable: LiveData<Boolean> = Transformations.map(currentUser) {
        it != null
    }
    val feedbackSentEvent = SingleLiveEvent<Boolean>()

    init {
        feedbackInteractor.feedbackSent = {
            feedbackSentEvent.value = true
        }
    }

    fun sendFeedback(text: String) {
        val userId = currentUser.value?.id ?: return
        feedbackInteractor.sendFeedback(userId, text)
    }

}