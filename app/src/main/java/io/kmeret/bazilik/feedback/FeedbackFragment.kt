package io.kmeret.bazilik.feedback

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.onClick
import io.kmeret.common.android.visibility
import kotlinx.android.synthetic.main.layout_feedback.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class FeedbackFragment : BaseFragment() {

    override val layoutResId: Int = R.layout.layout_feedback
    private val viewModel by viewModel<FeedbackViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeTitle(getString(R.string.contacts))
        sendFeedbackButton.onClick {
            viewModel.sendFeedback(feedbackView.text.toString())
            feedbackView.text?.clear() ?: return@onClick
        }
        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.feedbackAvailable.subscribe(this) {
                feedbackView.visibility(it)
                sendFeedbackButton.visibility(it)
            }
            it.feedbackSentEvent.subscribe(this) {
                if (it) requireContext().toast(getString(R.string.feedbackSent))
            }
        }
    }
}