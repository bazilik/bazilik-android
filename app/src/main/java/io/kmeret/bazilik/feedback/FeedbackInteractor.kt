package io.kmeret.bazilik.feedback

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiNewFeedback
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.FeedbackNotSentException

class FeedbackInteractor(private val apiService: ApiService,
                         private val tokenManager: TokenManager) : BaseInteractor() {


    var feedbackSent: (() -> Unit)? = null

    fun sendFeedback(userId: Long, text: String) {
        apiService.sendFeedback(tokenManager.getAccessToken(), ApiNewFeedback(userId, text)).request {
            if (!it.success) {
                onErrorCallback?.invoke(FeedbackNotSentException())
                return@request
            }

            feedbackSent?.invoke()
        }
    }

}