package io.kmeret.bazilik.navigation

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.support.v4.app.Fragment
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.message.MessageListInteractor
import io.kmeret.bazilik.shop.cart.CartInteractor
import io.kmeret.bazilik.storage.entity.AuthUser

class NavigationViewModel(private val drawerProvider: DrawerProvider,
                          cartInteractor: CartInteractor,
                          userInteractor: UserInteractor,
                          private val messageListInteractor: MessageListInteractor
) : BaseViewModel(listOf(cartInteractor, userInteractor, messageListInteractor)) {

    val currentFragment: MutableLiveData<Fragment> = MutableLiveData()

    val cartItemsCount = cartInteractor.getCartItemsCount()
    val unviewedMessagesCount = messageListInteractor.getUnviewedMessagesCount()

    val currentUser: LiveData<AuthUser?> = userInteractor.getAuthorized()
    val drawerItemList: LiveData<List<DrawerItem>> = Transformations.map(currentUser) { user ->
        when (user != null) {
            true -> drawerProvider.getUserMenu()
            false -> drawerProvider.getGuestMenu()
        }
    }
    val userDataVisible: LiveData<Boolean> = Transformations.map(currentUser) { user ->
        user != null
    }

    fun changeFragment(fragment: Fragment) {
        messageListInteractor.requestUnviewedMessageList()
        currentFragment.value = fragment
    }

    fun changeFragmentByMenuItemId(itemId: Long) {
        messageListInteractor.requestUnviewedMessageList()
        currentFragment.value = drawerProvider.resolveFragment(itemId) ?: return
    }

}