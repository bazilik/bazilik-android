package io.kmeret.bazilik.navigation

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object NavigationModule : Module {
    override fun invoke() = applicationContext {
        bean { DrawerProvider() }
        viewModel { NavigationViewModel(get(), get(), get(), get()) }
    }.invoke()
}