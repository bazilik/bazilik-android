package io.kmeret.bazilik.navigation

import io.kmeret.bazilik.R
import io.kmeret.bazilik.feedback.FeedbackFragment
import io.kmeret.bazilik.friends.FriendListFragment
import io.kmeret.bazilik.orders.OrderListFragment
import io.kmeret.bazilik.shop.cart.CartFragment
import io.kmeret.bazilik.shop.shoplist.ShopListFragment
import io.kmeret.bazilik.stocks.StockFragment

class DrawerProvider {

    private val signIn = DrawerItem(1, R.drawable.ic_signin, R.string.signIn)
    private val cart = DrawerItem(2, R.drawable.ic_cart, R.string.cart)
    private val restaurants = DrawerItem(3, R.drawable.ic_restaurants, R.string.shops)
    private val promotions = DrawerItem(4, R.drawable.ic_bonuses, R.string.stocks)
    private val contacts = DrawerItem(5, R.drawable.ic_phone, R.string.contacts)
    private val orders = DrawerItem(6, R.drawable.ic_orders, R.string.myOrders)
    private val signOut = DrawerItem(7, R.drawable.ic_exit, R.string.signOut)
    private val friends = DrawerItem(8, R.drawable.ic_people, R.string.friends)

    fun getGuestMenu() = listOf(signIn, restaurants, promotions, contacts)

    fun getUserMenu() = listOf(friends, orders, restaurants, promotions, contacts, signOut)

    fun resolveFragment(itemId: Long) = when (itemId) {
        2L -> CartFragment()
        3L -> ShopListFragment()
        4L -> StockFragment()
        5L -> FeedbackFragment()
        6L -> OrderListFragment()
        8L -> FriendListFragment()
        else -> null
    }

}