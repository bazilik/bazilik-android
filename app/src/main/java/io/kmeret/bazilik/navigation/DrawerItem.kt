package io.kmeret.bazilik.navigation

import io.kmeret.common.lists.Identifiable

data class DrawerItem(override var id: Long,
                      val iconResId: Int,
                      val textResId: Int) : Identifiable