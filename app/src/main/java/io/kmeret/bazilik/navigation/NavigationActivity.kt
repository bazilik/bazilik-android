package io.kmeret.bazilik.navigation

import android.Manifest
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.auth.view.AuthActivity
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import io.kmeret.bazilik.contacts.ContactsViewModel
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.message.MessageListFragment
import io.kmeret.bazilik.profile.view.ProfileFragment
import io.kmeret.bazilik.share.ShareVkInteractor
import io.kmeret.bazilik.shop.cart.CartFragment
import io.kmeret.bazilik.shop.shoplist.ShopListFragment
import io.kmeret.common.android.*
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.ic_dish_counter.*
import kotlinx.android.synthetic.main.ic_messages_counter.view.*
import kotlinx.android.synthetic.main.layout_navigation.*
import kotlinx.android.synthetic.main.template_drawer_item.view.*
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.android.inject

class NavigationActivity : BaseActivity() {

    companion object {
        const val SIGN_IN = "SIGN_IN"
    }

    private val navigationViewModel by viewModel<NavigationViewModel>()
    private val authViewModel by viewModel<AuthViewModel>()
    private val contactsViewModel by viewModel<ContactsViewModel>()
    private val shareVkInteractor by inject<ShareVkInteractor>()

    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var menuAdapter: RecyclerAdapter<DrawerItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_navigation)
        fragmentLayoutResId = R.id.fragmentLayout
        hamburgerVisible(true)
        backArrowVisible(true)
        initDrawer()
        initDrawerList()

        authViewModel.let {
            it.errorMessage.subscribe(this) { toast(it) }
            it.apiTokenRetrievedEvent.subscribe(this) { if (it) openInitFragment() }
            it.firebaseTokenRetrievedEvent.subscribe(this) { if (it) requestImportContacts() }
            it.retrieveTokens()
        }

        navigationViewModel.let {
            it.errorMessage.subscribe(this) { toast(it) }
            it.drawerItemList.subscribe(this) { menuAdapter.updateList(it) }
            it.userDataVisible.subscribe(this) { profileItem.visibility(it) }
            it.currentFragment.subscribe(this) { changeFragment(it, slideAnimation = false, clearStack = true) }
            it.currentUser.subscribe(this) {
                if (it == null) return@subscribe

                userAvatarView.loadWithUrl(it.avatarUrl)
                userNameView.text = it.name
                userPhoneView.text = it.phone
            }
        }
    }

    private fun openInitFragment() =
            changeFragment(ShopListFragment(), slideAnimation = false, clearStack = true)

    private fun initDrawer() {
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        profileItem.onClick {
            navigationViewModel.changeFragment(ProfileFragment())
            drawerLayout.closeDrawer(menuLayout)
        }
    }

    private fun initDrawerList() {
        menuAdapter = RecyclerAdapter(R.layout.template_drawer_item) { item, rootView ->
            rootView.run {
                itemText.text = getString(item.textResId)
                itemIcon.setImageVector(item.iconResId)
                onClick { onMenuItemClick(item.id) }
            }
        }
        menuListView.attachAdapter(menuAdapter)
        menuListView.isNestedScrollingEnabled = false
    }

    private fun requestImportContacts() {
        val bundle = intent.extras ?: return
        if (bundle.getBoolean(SIGN_IN)) return

        getPermissionManager().let {
            it.onPermissionsChecked = { it ->
                contactsViewModel.let {
                    it.errorMessage.subscribe(this) { toast(it) }
                    it.contactImportedEvent.subscribe(this) {
                        if (it) toast(R.string.contactsImported)
                    }
                    it.importContacts(this)
                }
            }
            it.requestPermission(Manifest.permission.READ_CONTACTS)
        }
    }

    private fun logout() {
        authViewModel.signOut()
        startActivity(intentFor<NavigationActivity>().clearTop())
    }

    private fun onMenuItemClick(menuItemId: Long) {
        when (menuItemId) {
            1L -> startActivity<AuthActivity>()
            7L -> logout()
        }
        navigationViewModel.changeFragmentByMenuItemId(menuItemId)
        drawerLayout.closeDrawer(menuLayout)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_navigation, menu)
        menu.findItem(R.id.itemCart).actionView.run {
            navigationViewModel.cartItemsCount.subscribe(this@NavigationActivity) {
                dishCountView.text = it.toString()
            }
            onClick {
                navigationViewModel.changeFragment(CartFragment())
                drawerLayout.closeDrawer(menuLayout)
            }
        }
        menu.findItem(R.id.itemMessages).also {
            it.isVisible = navigationViewModel.userDataVisible.value ?: false
            it.actionView.run {
                navigationViewModel.unviewedMessagesCount.subscribe(this@NavigationActivity) {
                    messageCountView.text = it.toString()
                }
                onClick {
                    navigationViewModel.changeFragment(MessageListFragment())
                    drawerLayout.closeDrawer(menuLayout)
                }
            }
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!shareVkInteractor.onActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (toggle.onOptionsItemSelected(item)) return true
        return super.onOptionsItemSelected(item)
    }

}
