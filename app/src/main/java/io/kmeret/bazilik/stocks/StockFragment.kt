package io.kmeret.bazilik.stocks

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.storage.entity.Stock
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_stock.*
import kotlinx.android.synthetic.main.template_stock.view.*
import org.koin.android.ext.android.inject

class StockFragment : BaseFragment() {

    override val layoutResId: Int = R.layout.layout_stock
    private val interactor by inject<StockInteractor>()
    private lateinit var stockListAdapter: RecyclerAdapter<Stock>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeTitle(getString(R.string.stocks))
        interactor.let {
            it.getStockList().subscribe(this) { stockListAdapter.updateList(it) }
            it.requestStockList()
        }

        initStockList()
    }

    private fun initStockList() {
        stockListAdapter = RecyclerAdapter(R.layout.template_stock) { item, rootView ->
            with(rootView) {
                stockPhotoView.loadWithUrl(item.photoUrl)

                val day = item.getDaysLeft().toInt()
                val hour = item.getHoursLeft().toInt()
                val minute = item.getMinutesLeft().toInt()

                val dayString = resources.getQuantityString(R.plurals.days, day)
                val hourString = resources.getQuantityString(R.plurals.hours, hour)
                val minuteString = resources.getQuantityString(R.plurals.minutes, minute)

                stockExpiredView.text = getString(R.string.left,
                        "$day $dayString, $hour $hourString, $minute $minuteString"
                )
            }
        }
        stockListView.attachAdapter(stockListAdapter)
    }
}