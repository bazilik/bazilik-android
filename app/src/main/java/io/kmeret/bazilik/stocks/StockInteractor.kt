package io.kmeret.bazilik.stocks

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.storage.dao.ShopDao
import io.kmeret.bazilik.storage.dao.StockDao

class StockInteractor(private val apiService: ApiService,
                      private val stockDao: StockDao,
                      private val shopDao: ShopDao,
                      private val tokenManager: TokenManager
) : BaseInteractor() {

    fun getStockList() = stockDao.findAll()

    fun requestStockList() {
        apiService.getStockList(tokenManager.getApiToken()).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }
            it.forEach {
                shopDao.save(it.shop.map())
                stockDao.save(it.map())
            }
        }
    }

}