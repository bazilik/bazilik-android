package io.kmeret.bazilik.stocks

import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object StockModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { StockInteractor(get(), get(), get(), get()) }
        }.invoke()
    }
}