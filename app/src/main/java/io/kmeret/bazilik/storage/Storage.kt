package io.kmeret.bazilik.storage

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import io.kmeret.bazilik.storage.dao.*
import io.kmeret.bazilik.storage.entity.*

@Database(entities = [
    (Shop::class),
    (Category::class),
    (Dish::class),
    (CartItem::class),
    (Keystore::class),
    (User::class),
    (AuthUser::class),
    (Address::class),
    (DeliveredDish::class),
    (Stock::class),
    (Contact::class),
    (Message::class),
    (Comment::class),
    (Friend::class)
], version = 1, exportSchema = false)
abstract class Storage : RoomDatabase() {
    abstract fun shopDao(): ShopDao
    abstract fun categoryDao(): CategoryDao
    abstract fun dishDao(): DishDao
    abstract fun cartItemDao(): CartItemDao
    abstract fun userDao(): UserDao
    abstract fun authUserDao(): AuthUserDao
    abstract fun keysDao(): KeystoreDao
    abstract fun addressDao(): AddressDao
    abstract fun deliveredDishDao(): DeliveredDishDao
    abstract fun stockDao(): StockDao
    abstract fun contactDao(): ContactDao
    abstract fun messageDao(): MessageDao
    abstract fun commentDao(): CommentDao
    abstract fun friendDao(): FriendDao
}
