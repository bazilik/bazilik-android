package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

//todo make temp cart item
@Entity(tableName = "cart_item")
data class CartItem(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(index = true)
        var id: Long? = null,
        var dishId: Long,
        var count: Int
)