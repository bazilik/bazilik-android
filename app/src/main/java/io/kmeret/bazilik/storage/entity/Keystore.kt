package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Keystore (
        @PrimaryKey
        @ColumnInfo(index = true)
        var id: Long,
        var apiToken: String,
        var accessToken: String
) {
    companion object {
        fun create() = Keystore(1, "", "")
    }
}
