package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Shop

@Dao
abstract class ShopDao : CrudDao<Shop>() {

    @Query("select * from shop where id=:id")
    abstract override fun getById(id: Long): Shop?

    @Query("select * from shop where id = :shopId")
    abstract fun findById(shopId: Long): LiveData<Shop>

    @Query("select * from shop")
    abstract fun findAll(): LiveData<List<Shop>>

}