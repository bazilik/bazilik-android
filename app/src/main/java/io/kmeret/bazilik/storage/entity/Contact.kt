package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity
class Contact(@PrimaryKey
              @ColumnInfo(index = true)
              override var id: Long,
              val name: String,
              val phone: String) : Identifiable