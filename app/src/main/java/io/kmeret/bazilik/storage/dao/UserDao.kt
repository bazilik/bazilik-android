package io.kmeret.bazilik.storage.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.User

@Dao
abstract class UserDao : CrudDao<User>() {

    @Query("select * from user where id = :id")
    abstract override fun getById(id: Long): User?

}