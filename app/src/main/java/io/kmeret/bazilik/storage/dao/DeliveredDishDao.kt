package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.kmeret.bazilik.orders.DeliveredDishDto
import io.kmeret.bazilik.storage.entity.DeliveredDish

@Dao
interface DeliveredDishDao {

    @Query("select del.id, del.count, del.createdAt as createdTime, " +
            "d.id as dishId, d.name as dishName, d.photoUrl as dishPhotoUrl, " +
            "d.price as dishPrice, d.commentCount as dishCommentCount, " +
            "s.id as shopId, s.name as shopName, s.logoUrl as shopLogoUrl " +
            "from delivered_dish del, dish d, category c, shop s " +
            "where del.dishId = d.id " +
            "and c.id = d.categoryId " +
            "and s.id = c.shopId " +
            "order by del.createdAt desc")
    fun getDeliveredList(): LiveData<List<DeliveredDishDto>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(item: DeliveredDish)

    @Query("delete from delivered_dish")
    fun deleteAll()

}