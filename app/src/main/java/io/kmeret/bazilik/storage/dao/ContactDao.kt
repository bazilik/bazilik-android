package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Contact

@Dao
abstract class ContactDao : CrudDao<Contact>() {

    @Query("select * from contact where id=:id")
    abstract override fun getById(id: Long): Contact?

    @Query("select c.id, c.name, c.phone " +
            "from contact c, friend f " +
            "where c.phone != f.phone order by c.name")
    abstract fun getAllWithoutFriends(): LiveData<List<Contact>>

    @Query("delete from contact")
    abstract fun deleteAll()

}