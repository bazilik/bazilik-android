package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity
data class Comment(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        var text: String,
        var createdAt: Long,
        @ColumnInfo(index = true)
        var userId: Long,
        @ColumnInfo(index = true)
        var dishId: Long
) : Identifiable