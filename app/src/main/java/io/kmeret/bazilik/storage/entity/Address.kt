package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity(foreignKeys = [
    (ForeignKey(entity = AuthUser::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("userId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE))
])
data class Address(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        var text: String,
        @ColumnInfo(index = true)
        var userId: Long
) : Identifiable