package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.AuthUser

@Dao
abstract class AuthUserDao : CrudDao<AuthUser>() {

    @Query("select * from auth_user where id=:id")
    abstract override fun getById(id: Long): AuthUser?

    @Query("select * from auth_user limit 1")
    abstract fun getLiveCurrent(): LiveData<AuthUser?>

    @Query("select * from auth_user limit 1")
    abstract fun getCurrent(): AuthUser?

    @Query("delete from auth_user")
    abstract fun deleteCurrent()

}