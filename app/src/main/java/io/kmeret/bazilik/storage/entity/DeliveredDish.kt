package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

//todo make cart item from server with server id
@Entity(tableName = "delivered_dish")
data class DeliveredDish(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(index = true)
        var id: Long? = null,
        var dishId: Long,
        var count: Int,
        var createdAt: Long
)