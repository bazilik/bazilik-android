package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.kmeret.bazilik.shop.cart.CartItemDto
import io.kmeret.bazilik.storage.entity.CartItem

@Dao
interface CartItemDao {

    @Query("select i.id, i.count, " +
            "d.id as dishId, d.name as dishName, d.photoUrl as dishPhotoUrl, d.price as dishPrice, " +
            "s.logoUrl as shopLogoUrl " +
            "from cart_item i, dish d, category c, shop s " +
            "where i.dishId = d.id " +
            "and c.id = d.categoryId " +
            "and s.id = c.shopId")
    fun getItems(): LiveData<List<CartItemDto>>

    @Query("select count(*) from cart_item")
    fun getItemsCount(): LiveData<Int>

    @Query("select dishId from cart_item")
    fun getItemIds(): List<Long>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(item: CartItem)

    @Query("update cart_item set count = :count where id = :itemId")
    fun updateItemCount(itemId: Long, count: Int)

    @Query("delete from cart_item where id = :itemId")
    fun deleteItem(itemId: Long)

    @Query("delete from cart_item")
    fun deleteAll()

}