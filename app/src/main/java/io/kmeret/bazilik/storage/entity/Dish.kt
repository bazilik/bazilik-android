package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity(foreignKeys = [
    (ForeignKey(entity = Category::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("categoryId"),
                onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE))
])
data class Dish(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        var name: String,
        var description: String,
        var price: Float,
        var photoUrl: String,
        var commentCount: Int,
        @ColumnInfo(index = true)
        var categoryId: Long
) : Identifiable