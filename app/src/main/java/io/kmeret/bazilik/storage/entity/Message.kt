package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity(foreignKeys = [
    (ForeignKey(entity = User::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("userFromId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE)),
    (ForeignKey(entity = AuthUser::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("userToId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE)),
    (ForeignKey(entity = Dish::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("dishId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE))
])
data class Message(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        @ColumnInfo(index = true)
        var userFromId: Long,
        @ColumnInfo(index = true)
        var userToId: Long,
        @ColumnInfo(index = true)
        var dishId: Long,
        var comment: String,
        var sentAt: Long,
        var viewed: Boolean
) : Identifiable