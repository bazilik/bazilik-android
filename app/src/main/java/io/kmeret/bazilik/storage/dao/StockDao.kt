package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Stock

@Dao
abstract class StockDao : CrudDao<Stock>() {

    @Query("select * from stock where id=:id")
    abstract override fun getById(id: Long): Stock?

    @Query("select * from stock order by expiredAt")
    abstract fun findAll(): LiveData<List<Stock>>

}