package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Friend
import io.kmeret.bazilik.storage.entity.User

@Dao
abstract class FriendDao : CrudDao<Friend>() {

    @Query("select * from friend where id = :id")
    abstract override fun getById(id: Long): Friend?

    @Query("select * from friend order by name")
    abstract fun getFriendList(): LiveData<List<User>>

    @Query("delete from user")
    abstract fun deleteAll()
}