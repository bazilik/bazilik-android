package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.message.DishMessageDto
import io.kmeret.bazilik.storage.entity.Message

@Dao
abstract class MessageDao : CrudDao<Message>() {

    @Query("select * from message where id = :id")
    abstract override fun getById(id: Long): Message?

    @Query("select count(*) from message where viewed = 0")
    abstract fun getUnviewedMessagesCount(): LiveData<Int>

    @Query("select m.id, m.comment, m.sentAt, " +
            "u.id as userFromId, u.name as userFromName, u.avatarUrl as userFromAvatarUrl, " +
            "s.id as shopId, s.name as shopName, s.logoUrl as shopLogoUrl, " +
            "d.id as dishId, d.name as dishName, " +
            "d.photoUrl as dishPhotoUrl, d.commentCount as dishCommentCount " +
            "from message m, dish d, category c, shop s, user u " +
            "where m.dishId = d.id " +
            "and c.id = d.categoryId " +
            "and s.id = c.shopId " +
            "and m.userFromId = u.id " +
            "and m.viewed = 0 " +
            "order by m.sentAt desc")
    abstract fun getMessageList(): LiveData<List<DishMessageDto>>

    @Query("update message set viewed = 1 where id in (:idList)")
    abstract fun viewMessageList(idList: List<Long>)

    @Query("delete from message")
    abstract fun deleteAll()

}