package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Address

@Dao
abstract class AddressDao : CrudDao<Address>() {

    @Query("select * from address where id=:id")
    abstract override fun getById(id: Long): Address?

    @Query("select * from address where id = :addressId")
    abstract fun findById(addressId: Long): LiveData<Address>

    @Query("select * from address")
    abstract fun findAll(): LiveData<List<Address>>

    @Query("delete from address where id = :addressId")
    abstract fun deleteById(addressId: Long)

    @Query("delete from address")
    abstract fun deleteAll()

}