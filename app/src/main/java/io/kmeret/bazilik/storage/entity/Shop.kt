package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity
data class Shop(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        var name: String,
        var logoUrl: String
) : Identifiable