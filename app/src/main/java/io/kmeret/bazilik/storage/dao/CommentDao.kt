package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.shop.dish.CommentDto
import io.kmeret.bazilik.storage.entity.Comment

@Dao
abstract class CommentDao : CrudDao<Comment>() {

    @Query("select * from comment where id = :id")
    abstract override fun getById(id: Long): Comment?

    @Query("select c.id, c.text, c.createdAt, " +
            "u.id as userId, u.name as userName, u.avatarUrl as userAvatarUrl " +
            "from comment c, user u " +
            "where c.dishId = :dishId " +
            "and c.userId = u.id " +
            "order by c.createdAt desc")
    abstract fun getCommentListByDishId(dishId: Long): LiveData<List<CommentDto>>

    @Query("delete from comment where dishId = :dishId")
    abstract fun deleteAllByDishId(dishId: Long)

}