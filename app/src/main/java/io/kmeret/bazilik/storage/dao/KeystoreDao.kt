package io.kmeret.bazilik.storage.dao

import android.arch.persistence.room.*
import io.kmeret.bazilik.storage.entity.Keystore

@Dao
interface KeystoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createKeyStore(keystore: Keystore)

    @Query("select * from keystore where id = 1")
    fun getKeyStore(): Keystore?

    @Update
    fun resetKeyStore(keystore: Keystore)

    @Query("update keystore set apiToken = :token where id = 1")
    fun saveApiToken(token: String)

    @Query("update keystore set accessToken = :token where id = 1")
    fun saveAccessToken(token: String)

}