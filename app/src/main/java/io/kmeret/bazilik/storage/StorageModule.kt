package io.kmeret.bazilik.storage

import android.arch.persistence.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object StorageModule : Module {
    override fun invoke() = applicationContext {
        bean {
            Room.databaseBuilder(
                    this.androidApplication(),
                    Storage::class.java,
                    "bazilik")
                    .allowMainThreadQueries()
                    .build()
        }
        bean { get<Storage>().shopDao() }
        bean { get<Storage>().categoryDao() }
        bean { get<Storage>().dishDao() }
        bean { get<Storage>().cartItemDao() }
        bean { get<Storage>().userDao() }
        bean { get<Storage>().authUserDao() }
        bean { get<Storage>().keysDao() }
        bean { get<Storage>().addressDao() }
        bean { get<Storage>().deliveredDishDao() }
        bean { get<Storage>().stockDao() }
        bean { get<Storage>().contactDao() }
        bean { get<Storage>().messageDao() }
        bean { get<Storage>().commentDao() }
        bean { get<Storage>().friendDao() }
    }.invoke()
}