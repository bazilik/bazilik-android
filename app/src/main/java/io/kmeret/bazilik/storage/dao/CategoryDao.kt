package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.storage.entity.Category

@Dao
abstract class CategoryDao : CrudDao<Category>() {

    @Query("select * from category where id = :id")
    abstract override fun getById(id: Long): Category?

    @Query("select * from category where shopId = :shopId")
    abstract fun findByShopId(shopId: Long): LiveData<List<Category>>

}