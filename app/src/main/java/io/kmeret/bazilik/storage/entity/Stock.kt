package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity(foreignKeys = [
    (ForeignKey(entity = Shop::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("shopId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE))
])
data class Stock(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        val description: String = "",
        val expiredAt: Long = 0,
        var photoUrl: String,
        @ColumnInfo(index = true)
        var shopId: Long
) : Identifiable {

    fun getMinutesLeft(): Long {
        val diff = getDiff() - this.getDaysLeft() * (24 * 60 * 60) - this.getHoursLeft() * (60 * 60)
        return diff / 60
    }

    fun getHoursLeft(): Long {
        val diff = getDiff() - this.getDaysLeft() * (24 * 60 * 60)
        return diff / (60 * 60)
    }

    fun getDaysLeft() = getDiff() / (24 * 60 * 60)

    private fun getDiff(): Long {
        val diff = (expiredAt - System.currentTimeMillis()) / 1000
        if (diff < 0) return 0
        return diff
    }

}