package io.kmeret.bazilik.storage.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import io.kmeret.bazilik.shop.dish.DishDto
import io.kmeret.bazilik.storage.entity.Dish

@Dao
abstract class DishDao : CrudDao<Dish>() {

    @Query("select * from dish where id=:id")
    abstract override fun getById(id: Long): Dish?

    @Query("select * from dish where id = :dishId")
    abstract fun findById(dishId: Long): LiveData<Dish>

    @Query("select d.id as dishId, d.name as dishName, d.description as dishDescription, " +
            "d.price as dishPrice, d.photoUrl as dishPhotoUrl, " +
            "s.id as shopId, s.name as shopName, s.logoUrl as shopLogoUrl " +
            "from dish d, category c, shop s " +
            "where d.id = :dishId and c.id = d.categoryId and s.id = c.shopId")
    abstract fun getByIdWithShop(dishId: Long): DishDto

    @Query("select d.id as dishId, d.name as dishName, d.description as dishDescription, " +
            "d.price as dishPrice, d.photoUrl as dishPhotoUrl, " +
            "s.id as shopId, s.name as shopName, s.logoUrl as shopLogoUrl " +
            "from dish d, category c, shop s " +
            "where d.id = :dishId and c.id = d.categoryId and s.id = c.shopId")
    abstract fun findByIdWithShop(dishId: Long): LiveData<DishDto>

    @Query("select * from dish where categoryId = :categoryId order by commentCount desc")
    abstract fun findByCategoryId(categoryId: Long): LiveData<List<Dish>>
}