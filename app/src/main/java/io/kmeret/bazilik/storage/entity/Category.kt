package io.kmeret.bazilik.storage.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import io.kmeret.common.lists.Identifiable

@Entity(foreignKeys = [
    (ForeignKey(entity = Shop::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("shopId"),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE))
])
data class Category(
        @PrimaryKey
        @ColumnInfo(index = true)
        override var id: Long,
        var name: String,
        var photoUrl: String,
        @ColumnInfo(index = true)
        var shopId: Long
) : Identifiable