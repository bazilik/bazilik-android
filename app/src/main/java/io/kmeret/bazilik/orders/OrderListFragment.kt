package io.kmeret.bazilik.orders

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.frame.toDateTimeString
import io.kmeret.bazilik.share.ShareDialog
import io.kmeret.bazilik.shop.cart.add.AddCartDialog
import io.kmeret.bazilik.shop.dish.DishFragment
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_orders.*
import kotlinx.android.synthetic.main.template_dish_delivered.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class OrderListFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_orders
    private val viewModel by viewModel<OrderListViewModel>()
    private lateinit var dishListAdapter: RecyclerAdapter<DeliveredDishDto>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.myOrders))

        initDishList()

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.deliveredList.subscribe(this) { dishListAdapter.updateList(it) }
            it.requestDeliveredDishList()
        }

    }

    private fun initDishList() {
        dishListAdapter = RecyclerAdapter(R.layout.template_dish_delivered) { item, rootView ->
            with(rootView) {
                shopLogoView.loadWithUrl(item.shopLogoUrl)
                dishPhotoView.loadWithUrl(item.dishPhotoUrl)

                orderCreatedAtView.text = item.createdTime.toDateTimeString()
                dishNameView.text = item.getDishNameCount()
                commentCountView.text = item.dishCommentCount.toString()

                addCartButton.onClick { openAddCartModal(item.dishId) }
                shareButton.onClick { openShareModal(item.dishId) }
                commentButton.onClick { changeFragment(DishFragment.create(item.dishId), false) }
                dishPhotoView.onClick { changeFragment(DishFragment.create(item.dishId), false) }

            }
        }
        dishListView.attachAdapter(dishListAdapter)
    }

    private fun openAddCartModal(dishId: Long) =
            AddCartDialog.create(dishId).also {
                it.show(childFragmentManager, AddCartDialog.TAG)
            }

    private fun openShareModal(dishId: Long) =
            ShareDialog.create(dishId).also {
                it.show(childFragmentManager, ShareDialog.TAG)
            }

}