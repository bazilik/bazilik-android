package io.kmeret.bazilik.orders

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.UserNotAuthException
import io.kmeret.bazilik.storage.dao.*
import io.kmeret.bazilik.storage.entity.DeliveredDish

class OrderListInteractor(private val apiService: ApiService,
                          private val tokenManager: TokenManager,
                          private val shopDao: ShopDao,
                          private val categoryDao: CategoryDao,
                          private val dishDao: DishDao,
                          private val deliveredDishDao: DeliveredDishDao,
                          private val authUserDao: AuthUserDao
) : BaseInteractor() {

    fun getDeliveredDishList() = deliveredDishDao.getDeliveredList()

    fun requestDeliveredDishList() {
        val user = authUserDao.getCurrent()
        if (user == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        apiService.getDeliveredDishList(tokenManager.getAccessToken(), user.id).request {

            deliveredDishDao.deleteAll()

            it.forEach {
                it.dish.let {
                    shopDao.save(it.category.shop.map())
                    categoryDao.save(it.category.map())
                    dishDao.save(it.map())
                }
                val delivered = DeliveredDish(dishId = it.dish.id,
                        count = it.count,
                        createdAt = it.createdAt)
                deliveredDishDao.insert(delivered)
            }
        }
    }

}