package io.kmeret.bazilik.orders

import android.arch.lifecycle.LiveData
import io.kmeret.bazilik.frame.BaseViewModel

class OrderListViewModel(private val orderListInteractor: OrderListInteractor)
    : BaseViewModel(listOf(orderListInteractor)) {

    val deliveredList: LiveData<List<DeliveredDishDto>> = orderListInteractor.getDeliveredDishList()

    fun requestDeliveredDishList() = orderListInteractor.requestDeliveredDishList()

}