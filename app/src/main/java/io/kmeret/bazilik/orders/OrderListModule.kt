package io.kmeret.bazilik.orders

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object OrderListModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { OrderListInteractor(get(), get(), get(), get(), get(), get(), get()) }
            viewModel { OrderListViewModel(get()) }
        }.invoke()
    }
}