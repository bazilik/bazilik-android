package io.kmeret.bazilik.orders

import io.kmeret.common.lists.Identifiable

data class DeliveredDishDto(
        override val id: Long = 0,
        val count: Int,
        var createdTime: Long = 0,

        val dishId: Long,
        val dishName: String,
        val dishPhotoUrl: String,
        val dishPrice: Float,
        val dishCommentCount: Int,

        val shopId: Long,
        val shopName: String,
        val shopLogoUrl: String

) : Identifiable {
    fun getDishNameCount() = "$dishName x$count"
}