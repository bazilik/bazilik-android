package io.kmeret.bazilik.shop.cart.add

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.storage.entity.Dish

class AddCartViewModel(private val addCartInteractor: AddCartInteractor) :
        BaseViewModel(listOf(addCartInteractor)) {

    val dishId = MutableLiveData<Long>()
    val dish: LiveData<Dish> = Transformations.switchMap(dishId) { addCartInteractor.getDish(it) }
    val dishCount = MutableLiveData<Int>()
    val dishAmount = MediatorLiveData<Float>()

    init {
        dishCount.value = 1
        dishAmount.addSource(dish) {
            val price = it?.price ?: return@addSource
            val count = dishCount.value ?: return@addSource
            dishAmount.value = price * count
        }
        dishAmount.addSource(dishCount) {
            val price = dish.value?.price ?: return@addSource
            val count = it ?: return@addSource
            dishAmount.value = price * count
        }
    }

    fun changeCount(delta: Int) {
        val newCount = dishCount.value!! + delta
        if (newCount < 1) return
        dishCount.value = newCount
    }

    fun addDishToCart() {
        addCartInteractor.addItemToCart(dishId.value!!, dishCount.value!!)
    }

}
