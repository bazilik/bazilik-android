package io.kmeret.bazilik.shop.dish

import io.kmeret.common.lists.Identifiable

data class CommentDto(

        override val id: Long = 0,
        val text: String,
        var createdAt: Long = 0,

        val userId: Long,
        val userName: String,
        val userAvatarUrl: String

) : Identifiable