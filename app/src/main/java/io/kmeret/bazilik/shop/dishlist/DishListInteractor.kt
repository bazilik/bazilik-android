package io.kmeret.bazilik.shop.dishlist

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.storage.dao.DishDao

class DishListInteractor(private val dishDao: DishDao,
                         private val tokenManager: TokenManager,
                         private val apiService: ApiService
) : BaseInteractor() {

    fun getDishList(categoryId: Long) = dishDao.findByCategoryId(categoryId)

    fun requestDishList(categoryId: Long) {
        apiService.getDishList(tokenManager.getApiToken(), categoryId).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }

            dishDao.saveAll(it.map { it.map() })
        }
    }
}