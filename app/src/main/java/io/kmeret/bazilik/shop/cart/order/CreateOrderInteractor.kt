package io.kmeret.bazilik.shop.cart.order

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiNewCartItem
import io.kmeret.bazilik.api.model.ApiNewOrder
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.OrderNotSentException
import io.kmeret.bazilik.shop.cart.CartItemDto
import io.kmeret.bazilik.storage.dao.CartItemDao

class CreateOrderInteractor(private val apiService: ApiService,
                            private val tokenManager: TokenManager,
                            private val cartItemDao: CartItemDao
) : BaseInteractor() {

    var onOrderSent: (() -> Unit)? = null

    fun sendOrder(addressId: Long, comment: String, cartItemList: List<CartItemDto>) {
        val apiNewCartItemList = cartItemList.map { ApiNewCartItem(it.dishId, it.count) }
        val apiNewOrder = ApiNewOrder(addressId, comment, apiNewCartItemList)
        apiService.sendOrder(tokenManager.getAccessToken(), apiNewOrder).request {
            if (!it.success) {
                onErrorCallback?.invoke(OrderNotSentException())
                return@request
            }

            cartItemDao.deleteAll()
            onOrderSent?.invoke()
        }
    }

}