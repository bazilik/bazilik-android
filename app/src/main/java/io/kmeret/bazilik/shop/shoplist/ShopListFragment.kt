package io.kmeret.bazilik.shop.shoplist

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.shop.shop.ShopFragment
import io.kmeret.bazilik.storage.entity.Shop
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_shop_list.*
import kotlinx.android.synthetic.main.template_shop.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class ShopListFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_shop_list
    private val viewModel by viewModel<ShopListViewModel>()
    private lateinit var shopAdapter: RecyclerAdapter<Shop>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.shops))
        swipeLayout.setOnRefreshListener { viewModel.refresh() }
        initShopList()

        viewModel.let {
            it.shopList.subscribe(this) {
                showShopList(it)
                swipeLayout.isRefreshing = false
            }
            it.errorMessage.subscribe(this) {
                requireContext().toast(it)
                swipeLayout.isRefreshing = false
            }
            it.refresh()
        }
    }

    private fun initShopList() {
        shopAdapter = RecyclerAdapter(R.layout.template_shop) { item, rootView ->
            rootView.run {
                shopLogoView.loadWithUrl(item.logoUrl)
                shopLayout.onClick { openShopDetail(item.id) }
            }
        }
        shopListView.attachAdapter(shopAdapter)
    }

    private fun showShopList(list: List<Shop>) {
        shopAdapter.updateList(list)
    }

    private fun openShopDetail(shopId: Long) = changeFragment(ShopFragment.create(shopId))

}
