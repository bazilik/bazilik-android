package io.kmeret.bazilik.shop.cart.order

import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.PopupMenu
import io.kmeret.bazilik.R
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.profile.viewmodel.ProfileViewModel
import io.kmeret.bazilik.shop.cart.CartItemDto
import io.kmeret.bazilik.shop.cart.CartViewModel
import io.kmeret.bazilik.shop.shoplist.ShopListFragment
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.android.onTextChanged
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_order_add.*
import kotlinx.android.synthetic.main.template_cart_item_order.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.sharedViewModel

class CreateOrderFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_order_add

    private val profileViewModel by sharedViewModel<ProfileViewModel>()
    private val cartViewModel by sharedViewModel<CartViewModel>()

    private lateinit var addressPopup: PopupMenu
    private val menuItemMap = HashMap<Int, Long>()

    private lateinit var cartItemListAdapter: RecyclerAdapter<CartItemDto>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.ordering))

        profileViewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.addressList.subscribe(this) {
                if (it.isEmpty()) {
                    profileViewModel.requestAddressList()
                    return@subscribe
                }

                cartViewModel.changeCurrentAddress(it.first().id)

                it.forEachIndexed { index, address ->
                    addressPopup.menu.add(1, index, index, address.text)
                    menuItemMap[index] = address.id
                }
            }
        }

        cartViewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.currentAddress.subscribe(this) {
                addressDropdownView.text = it.text
            }
            it.cartItemList.subscribe(this) { cartItemListAdapter.updateList(it) }
            it.cartAmount.subscribe(this) {
                cartAmountView.text = getString(R.string.orderAmount, it.toString())
            }
            it.orderSentEvent.subscribe(this) {
                if (it) {
                    requireContext().toast(R.string.orderSent)
                    changeFragment(ShopListFragment(), false)
                }
            }
        }

        orderCommentView.onTextChanged { text, _ -> cartViewModel.comment.value = text }
        addressDropdownView.onClick { addressPopup.show() }
        sendOrderButton.onClick { cartViewModel.sendOrder() }

        initAddressPopup()
        initCartItemList()
    }

    private fun initAddressPopup() {
        val wrapper = ContextThemeWrapper(requireContext(), R.style.LongPopup)
        addressPopup = PopupMenu(wrapper, addressDropdownView).apply {
            setOnMenuItemClickListener {
                cartViewModel.changeCurrentAddress(menuItemMap.getValue(it.itemId))
                true
            }
        }
    }

    private fun initCartItemList() {
        cartItemListAdapter = RecyclerAdapter(R.layout.template_cart_item_order) { item, rootView ->
            with(rootView) {
                cartItemDishNameView.text = item.dishName
                cartItemAmountView.text = item.getTotalInfo()
            }
        }
        cartItemListView.attachAdapter(cartItemListAdapter)
    }

}