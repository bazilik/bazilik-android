package io.kmeret.bazilik.shop.cart.add

import android.arch.lifecycle.LiveData
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.storage.dao.CartItemDao
import io.kmeret.bazilik.storage.dao.DishDao
import io.kmeret.bazilik.storage.entity.CartItem
import io.kmeret.bazilik.storage.entity.Dish

class AddCartInteractor(private val dishDao: DishDao,
                        private val cartItemDao: CartItemDao) : BaseInteractor() {

    fun getDish(dishId: Long): LiveData<Dish> = dishDao.findById(dishId)

    fun addItemToCart(dishId: Long, count: Int) =
            cartItemDao.insert(CartItem(dishId = dishId, count = count))
}