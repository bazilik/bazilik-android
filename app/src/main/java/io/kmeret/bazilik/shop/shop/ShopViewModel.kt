package io.kmeret.bazilik.shop.shop

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.storage.entity.Category
import io.kmeret.bazilik.storage.entity.Shop

class ShopViewModel(private val shopInteractor: ShopInteractor)
    : BaseViewModel(listOf(shopInteractor)) {

    val shopId: MutableLiveData<Long> = MutableLiveData()
    val shop: LiveData<Shop> =
            Transformations.switchMap(shopId) { shopInteractor.getShop(it) }
    val categoryList: LiveData<List<Category>> =
            Transformations.switchMap(shopId) { shopInteractor.getCategoryList(it) }

    fun refresh() {
        val currentShopId = shopId.value ?: return
        shopInteractor.requestCategoryList(currentShopId)
    }

}