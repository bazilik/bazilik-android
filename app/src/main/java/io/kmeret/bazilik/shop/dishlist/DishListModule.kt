package io.kmeret.bazilik.shop.dishlist

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object DishListModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { DishListInteractor(get(), get(),get()) }
            viewModel { DishListViewModel(get()) }
        }.invoke()
    }
}