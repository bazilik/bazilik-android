package io.kmeret.bazilik.shop.dish

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object DishModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { DishInteractor(get(), get(), get(), get(), get(), get()) }
            viewModel { DishViewModel(get(), get()) }
        }.invoke()
    }
}