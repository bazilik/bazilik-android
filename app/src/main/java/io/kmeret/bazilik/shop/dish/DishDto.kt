package io.kmeret.bazilik.shop.dish

data class DishDto(
        val dishId: Long,
        val dishName: String,
        val dishDescription: String,
        val dishPrice: Float,
        val dishPhotoUrl: String,

        val shopId: Long,
        val shopName: String,
        val shopLogoUrl: String
)