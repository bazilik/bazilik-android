package io.kmeret.bazilik.shop.shop

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.storage.dao.CategoryDao
import io.kmeret.bazilik.storage.dao.ShopDao

class ShopInteractor(private val shopDao: ShopDao,
                     private val categoryDao: CategoryDao,
                     private val tokenManager: TokenManager,
                     private val apiService: ApiService
) : BaseInteractor() {

    fun getShop(shopId: Long) = shopDao.findById(shopId)

    fun getCategoryList(shopId: Long) = categoryDao.findByShopId(shopId)

    fun requestCategoryList(shopId: Long) {
        apiService.getCategoryList(tokenManager.getApiToken(), shopId).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }

            categoryDao.saveAll(it.map { it.map() })
        }
    }

}
