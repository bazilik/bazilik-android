package io.kmeret.bazilik.shop.dishlist

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.storage.entity.Dish

class DishListViewModel(private val dishListInteractor: DishListInteractor)
    : BaseViewModel(listOf(dishListInteractor)) {

    private val currentCategoryId: MutableLiveData<Long> = MutableLiveData()
    val dishList: LiveData<List<Dish>> =
            Transformations.switchMap(currentCategoryId) { dishListInteractor.getDishList(it) }

    fun requestDishList(categoryId: Long) {
        currentCategoryId.value = categoryId
        dishListInteractor.requestDishList(categoryId)
    }

}