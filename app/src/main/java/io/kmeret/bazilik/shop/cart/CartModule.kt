package io.kmeret.bazilik.shop.cart

import io.kmeret.bazilik.shop.cart.order.CreateOrderInteractor
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object CartModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { CartInteractor(get(), get(), get(), get(), get(), get()) }
            bean { CreateOrderInteractor(get(), get(), get()) }
            viewModel { CartViewModel(get(), get(), get(), get()) }
        }.invoke()
    }
}