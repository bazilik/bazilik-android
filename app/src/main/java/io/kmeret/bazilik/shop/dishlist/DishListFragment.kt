package io.kmeret.bazilik.shop.dishlist

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import android.widget.LinearLayout
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.share.ShareDialog
import io.kmeret.bazilik.shop.cart.add.AddCartDialog
import io.kmeret.bazilik.shop.dish.DishFragment
import io.kmeret.bazilik.storage.entity.Dish
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_dish_list.*
import kotlinx.android.synthetic.main.template_dish.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class DishListFragment : BaseFragment() {

    companion object {
        const val CATEGORY_ID = "CATEGORY_ID"
        const val CATEGORY_NAME = "CATEGORY_NAME"
        fun create(categoryId: Long, categoryName: String) = DishListFragment().apply {
            arguments = Bundle().apply {
                putLong(CATEGORY_ID, categoryId)
                putString(CATEGORY_NAME, categoryName)
            }
        }
    }

    override val layoutResId = R.layout.layout_dish_list
    private val viewModel by viewModel<DishListViewModel>()
    private lateinit var dishAdapter: RecyclerAdapter<Dish>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title = arguments?.getString(CATEGORY_NAME) ?: return
        changeTitle(title)

        initDishList()

        val categoryId = arguments?.getLong(CATEGORY_ID) ?: return
        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.dishList.subscribe(this) { dishAdapter.updateList(it) }
            it.requestDishList(categoryId)
        }

    }

    private fun initDishList() {
        dishAdapter = RecyclerAdapter(R.layout.template_dish) { item, rootView ->
            with(rootView) {
                dishPhotoView.loadWithUrl(item.photoUrl)
                dishNameView.text = item.name
                dishDescriptionView.text = item.description
                dishPriceView.text = getString(R.string.dishPrice, item.price.toString())
                commentCountView.text = item.commentCount.toString()

                addCartButton.onClick { addDishToCart(item.id) }
                shareButton.onClick { openShareModal(item.id) }
                dishPhotoView.onClick { changeFragment(DishFragment.create(item.id)) }
                commentButton.onClick { changeFragment(DishFragment.create(item.id)) }

            }
        }
        dishListView.apply {
            attachAdapter(dishAdapter)
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
    }

    private fun addDishToCart(dishId: Long) {
        AddCartDialog.create(dishId).also {
            it.show(childFragmentManager, AddCartDialog.TAG)
        }
    }

    private fun openShareModal(dishId: Long) =
            ShareDialog.create(dishId).also {
                it.show(childFragmentManager, ShareDialog.TAG)
            }

}