package io.kmeret.bazilik.shop.dish

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.frame.BaseViewModel

class DishViewModel(private val dishInteractor: DishInteractor,
                    userInteractor: UserInteractor
) : BaseViewModel(listOf(dishInteractor, userInteractor)) {

    private val currentDishId = MutableLiveData<Long>()
    val dish: LiveData<DishDto> = Transformations.switchMap(currentDishId) {
        dishInteractor.getDishById(it)
    }
    val commentList: LiveData<List<CommentDto>> = Transformations.switchMap(currentDishId) {
        dishInteractor.getCommentListByDishId(it)
    }
    val sendCommentAvailable: LiveData<Boolean> = Transformations.map(userInteractor.getAuthorized()) {
        it != null
    }

    fun requestDish(dishId: Long) {
        currentDishId.value = dishId
        dishInteractor.requestCommentList(dishId)
    }

    fun sendComment(text: String) {
        val dishId = currentDishId.value ?: return
        dishInteractor.sendComment(dishId, text)
    }

}