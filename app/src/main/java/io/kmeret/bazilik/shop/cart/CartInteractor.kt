package io.kmeret.bazilik.shop.cart

import android.arch.lifecycle.LiveData
import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.storage.dao.CartItemDao
import io.kmeret.bazilik.storage.dao.CategoryDao
import io.kmeret.bazilik.storage.dao.DishDao
import io.kmeret.bazilik.storage.dao.ShopDao

class CartInteractor(private val cartItemDao: CartItemDao,
                     private val dishDao: DishDao,
                     private val shopDao: ShopDao,
                     private val categoryDao: CategoryDao,
                     private val tokenManager: TokenManager,
                     private val apiService: ApiService
) : BaseInteractor() {

    fun getCartItemList(): LiveData<List<CartItemDto>> = cartItemDao.getItems()

    fun getCartItemsCount(): LiveData<Int> = cartItemDao.getItemsCount()

    fun deleteCartItem(itemId: Long) = cartItemDao.deleteItem(itemId)

    fun requestDishList() {
        val idList = cartItemDao.getItemIds()
        if (idList.isEmpty()) return

        apiService.getDishListByIds(tokenManager.getApiToken(), idList).request {
            it.forEach {
                shopDao.save(it.category.shop.map())
                categoryDao.save(it.category.map())
                dishDao.save(it.map())
            }
        }
    }

    fun updateItem(item: CartItemDto) {
        cartItemDao.updateItemCount(item.id, item.count)
    }

}