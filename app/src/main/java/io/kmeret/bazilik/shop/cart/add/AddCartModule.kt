package io.kmeret.bazilik.shop.cart.add

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object AddCartModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { AddCartInteractor(get(), get()) }
            viewModel { AddCartViewModel(get()) }
        }.invoke()
    }
}