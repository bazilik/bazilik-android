package io.kmeret.bazilik.shop.shop

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object ShopModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { ShopInteractor(get(), get(), get(), get()) }
            viewModel { ShopViewModel(get()) }
        }.invoke()
    }
}