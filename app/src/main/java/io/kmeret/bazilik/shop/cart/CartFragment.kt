package io.kmeret.bazilik.shop.cart

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.view.View
import android.widget.LinearLayout
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.auth.view.AuthActivity
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.shop.cart.order.CreateOrderFragment
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.android.visibility
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_cart.*
import kotlinx.android.synthetic.main.template_cart_item.view.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.sharedViewModel

class CartFragment : BaseFragment() {

    override val layoutResId: Int = R.layout.layout_cart
    private val viewModel by sharedViewModel<CartViewModel>()
    private lateinit var cartItemAdapter: RecyclerAdapter<CartItemDto>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.cartItemList.subscribe(this) {
                cartItemAdapter.updateList(it)
            }
            it.cartAmount.subscribe(this) {
                cartAmountView.text = getString(R.string.orderAmount, it.toString())
            }
            it.createOrderAvailable.subscribe(this) { paymentButton.visibility(it) }
            it.currentUser.subscribe(this) {}
            it.getCartItemList()
        }

        paymentButton.onClick {
            if (viewModel.currentUser.value == null) {
                with(requireActivity()) { startActivity<AuthActivity>() }
                return@onClick
            }
            changeFragment(CreateOrderFragment())
        }

        changeTitle(getString(R.string.cart))
        initCartItemList()
    }

    private fun initCartItemList() {
        cartItemAdapter = RecyclerAdapter(R.layout.template_cart_item) { item, rootView ->
            with(rootView) {
                shopLogoView.loadWithUrl(item.shopLogoUrl)
                dishPhotoView.loadWithUrl(item.dishPhotoUrl)
                dishNameView.text = item.dishName
                dishCountView.text = getString(R.string.itemCount, item.count.toString())
                dishAmountView.text = getString(R.string.itemAmount, item.getAmount())
                deleteItemButton.onClick { viewModel.deleteItem(item.id) }
                countLeftButton.onClick { viewModel.updateCount(-1, item) }
                countRightButton.onClick { viewModel.updateCount(1, item) }
            }
        }
        cartItemListView.apply {
            attachAdapter(cartItemAdapter)
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
    }

}