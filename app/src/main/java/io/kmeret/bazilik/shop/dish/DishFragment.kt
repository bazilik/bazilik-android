package io.kmeret.bazilik.shop.dish

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.frame.toDateTimeString
import io.kmeret.bazilik.share.ShareDialog
import io.kmeret.bazilik.shop.cart.add.AddCartDialog
import io.kmeret.common.android.*
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_dish.*
import kotlinx.android.synthetic.main.template_comment.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class DishFragment : BaseFragment() {

    companion object {
        const val DISH_ID = "DISH_ID"
        fun create(dishId: Long) = DishFragment().apply {
            arguments = Bundle().apply {
                putLong(DISH_ID, dishId)
            }
        }
    }

    override val layoutResId = R.layout.layout_dish
    private val viewModel by viewModel<DishViewModel>()
    private lateinit var commentListAdapter: RecyclerAdapter<CommentDto>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.comments))
        initCommentList()

        val dishId = arguments?.getLong(DISH_ID) ?: return

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.sendCommentAvailable.subscribe(this) { commentLayout.visibility(it) }
            it.dish.subscribe(this) { initDish(it) }
            it.commentList.subscribe(this) { commentListAdapter.updateList(it) }
            it.requestDish(dishId)
        }

        sendCommentButton.onClick { sendComment() }
    }

    private fun initDish(dto: DishDto) {
        dishPhotoView.loadWithUrl(dto.dishPhotoUrl)
        shopLogoView.loadWithUrl(dto.shopLogoUrl)

        dishNameView.text = dto.dishName
        dishDescriptionView.text = dto.dishDescription
        dishPriceView.text = getString(R.string.dishPrice, dto.dishPrice.toString())

        addCartButton.onClick { openAddCartModal(dto.dishId) }
        shareButton.onClick { openShareModal(dto.dishId) }
    }

    private fun initCommentList() {
        commentListAdapter = RecyclerAdapter(R.layout.template_comment) { item, rootView ->
            with(rootView) {
                userAvatarView.loadWithUrl(item.userAvatarUrl)
                userNameView.text = item.userName
                commentCreatedAtView.text = item.createdAt.toDateTimeString()
                commentTextView.text = item.text
            }
        }
        commentListView.attachAdapter(commentListAdapter)
    }

    private fun sendComment() {
        viewModel.sendComment(commentEditTextView.text.toString())
        commentEditTextView.keyboardVisible(false)
        commentEditTextView.text?.clear() ?: return
    }

    private fun openAddCartModal(dishId: Long) =
            AddCartDialog.create(dishId).also {
                it.show(childFragmentManager, AddCartDialog.TAG)
            }

    private fun openShareModal(dishId: Long) =
            ShareDialog.create(dishId).also {
                it.show(childFragmentManager, ShareDialog.TAG)
            }

}