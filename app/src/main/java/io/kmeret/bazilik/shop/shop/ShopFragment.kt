package io.kmeret.bazilik.shop.shop

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.shop.dishlist.DishListFragment
import io.kmeret.bazilik.storage.entity.Category
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachGridAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_shop.*
import kotlinx.android.synthetic.main.template_category.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class ShopFragment : BaseFragment() {

    companion object {
        const val SHOP_ID = "SHOP_ID"
        fun create(shopId: Long) = ShopFragment().apply {
            arguments = Bundle().apply {
                putLong(SHOP_ID, shopId)
            }
        }
    }

    override val layoutResId = R.layout.layout_shop
    private val viewModel by viewModel<ShopViewModel>()
    private lateinit var categoryAdapter: RecyclerAdapter<Category>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.shop.subscribe(this) {
                changeTitle(it.name)
                shopLogoView.loadWithUrl(it.logoUrl)
            }
            it.categoryList.subscribe(this) {
                categoryAdapter.updateList(it)
            }
        }
        initCategoryList()
    }

    override fun onResume() {
        super.onResume()
        viewModel.shopId.value = arguments?.getLong(SHOP_ID) ?: return
        viewModel.refresh()
    }

    private fun initCategoryList() {
        categoryAdapter = RecyclerAdapter(R.layout.template_category) { item, rootView ->
            with(rootView) {
                categoryNameView.text = item.name
                categoryPhotoView.loadWithUrl(item.photoUrl)
                categoryCardView.onClick { openDishList(item.id, item.name) }
            }
        }
        categoryListView.apply {
            isNestedScrollingEnabled = false
            attachGridAdapter(categoryAdapter)
        }
    }

    private fun openDishList(categoryId: Long, categoryName: String) =
            changeFragment(DishListFragment.create(categoryId, categoryName))

}