package io.kmeret.bazilik.shop.shoplist

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object ShopListModule : Module {
    override fun invoke() = applicationContext {
        bean { ShopListInteractor(get(), get(), get()) }
        viewModel { ShopListViewModel(get()) }
    }.invoke()
}
