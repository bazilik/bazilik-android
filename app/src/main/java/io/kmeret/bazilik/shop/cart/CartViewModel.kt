package io.kmeret.bazilik.shop.cart

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.frame.SingleLiveEvent
import io.kmeret.bazilik.profile.model.AddressInteractor
import io.kmeret.bazilik.shop.cart.order.CreateOrderInteractor
import io.kmeret.bazilik.storage.entity.Address

class CartViewModel(private val cartInteractor: CartInteractor,
                    private val addressInteractor: AddressInteractor,
                    private val createOrderInteractor: CreateOrderInteractor,
                    userInteractor: UserInteractor) :
        BaseViewModel(listOf(cartInteractor, addressInteractor, createOrderInteractor, userInteractor)) {

    val cartItemList: LiveData<List<CartItemDto>> = cartInteractor.getCartItemList()
    val cartAmount: LiveData<Float> = Transformations.map(cartItemList) {
        it.fold(0f) { amount, item -> amount + item.count * item.dishPrice }
    }

    val currentUser = userInteractor.getAuthorized()
    private val currentAddressId = MutableLiveData<Long>()
    val currentAddress: LiveData<Address> = Transformations.switchMap(currentAddressId) {
        addressInteractor.getAddressById(it)
    }

    val comment = MutableLiveData<String>()
    val orderSentEvent = SingleLiveEvent<Boolean>()
    val createOrderAvailable: LiveData<Boolean> = Transformations.map(cartItemList) { it.isNotEmpty() }

    init {
       createOrderInteractor.apply {
           onOrderSent = { orderSentEvent.value = true }
       }
    }

    fun getCartItemList() = cartInteractor.requestDishList()

    fun updateCount(delta: Int, item: CartItemDto) {
        item.changeCount(delta)
        cartInteractor.updateItem(item)
    }

    fun deleteItem(id: Long) = cartInteractor.deleteCartItem(id)

    fun changeCurrentAddress(addressId: Long) {
        currentAddressId.value = addressId
    }

    fun sendOrder() {
        val addressId = currentAddressId.value ?: return
        val cartItemList = cartItemList.value ?: return
        if (cartItemList.isEmpty()) return
        val comment = comment.value ?: ""
        createOrderInteractor.sendOrder(addressId, comment, cartItemList)
    }

}