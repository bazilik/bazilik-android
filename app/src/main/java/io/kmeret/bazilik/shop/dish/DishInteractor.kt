package io.kmeret.bazilik.shop.dish

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiNewComment
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.frame.UserNotAuthException
import io.kmeret.bazilik.storage.dao.AuthUserDao
import io.kmeret.bazilik.storage.dao.CommentDao
import io.kmeret.bazilik.storage.dao.DishDao
import io.kmeret.bazilik.storage.dao.UserDao

class DishInteractor(private val dishDao: DishDao,
                     private val commentDao: CommentDao,
                     private val userDao: UserDao,
                     private val authUserDao: AuthUserDao,
                     private val apiService: ApiService,
                     private val tokenManager: TokenManager
) : BaseInteractor() {

    fun getDishById(dishId: Long) = dishDao.findByIdWithShop(dishId)

    fun getCommentListByDishId(dishId: Long) =
            commentDao.getCommentListByDishId(dishId)

    fun requestCommentList(dishId: Long) {
        apiService.getCommentListByDishId(tokenManager.getApiToken(), dishId).request {
            commentDao.deleteAllByDishId(dishId)

            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }

            it.forEach {
                userDao.save(it.user.mapUser())
                commentDao.save(it.map(dishId))
            }
        }
    }

    fun sendComment(dishId: Long, text: String) {
        val user = authUserDao.getCurrent()
        if (user == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        val comment = ApiNewComment(text, user.id, dishId)
        apiService.sendComment(tokenManager.getAccessToken(), comment).request {
            commentDao.insert(it.map(dishId))
        }
    }
}