package io.kmeret.bazilik.shop.cart.add

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.common.android.onClick
import io.kmeret.common.android.toEditable
import kotlinx.android.synthetic.main.layout_cart_add.view.*
import kotlinx.android.synthetic.main.layout_navigation.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class AddCartDialog : DialogFragment() {

    companion object {
        const val TAG = "AddCartDialog"
        const val DISH_ID = "DISH_ID"

        fun create(dishId: Long) = AddCartDialog().apply {
            arguments = Bundle().apply {
                putLong(DISH_ID, dishId)
            }
        }
    }

    private val viewModel by viewModel<AddCartViewModel>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val layout = LayoutInflater.from(activity)
                .inflate(R.layout.layout_cart_add, drawerLayout, false)

        layout.run {
            countLeftButton.onClick { viewModel.changeCount(-1) }
            countRightButton.onClick { viewModel.changeCount(1) }
        }

        viewModel.let {
            it.dishId.value = arguments?.getLong(DISH_ID)
            it.dish.subscribe(this) {
                layout.run {
                    dishPhotoView.loadWithUrl(it.photoUrl)
                    dishNameView.text = it.name
                }
            }
            it.dishCount.subscribe(this) {
                layout.dishCountView.text = getString(R.string.itemCount, it.toString().toEditable())
            }
            it.dishAmount.subscribe(this) {
                layout.dishAmountView.text = getString(R.string.itemAmount, it.toString())
            }
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
        }

        return AlertDialog.Builder(activity)
                .setView(layout)
                .setTitle(R.string.addCart)
                .setPositiveButton(R.string.add) { _, _ -> viewModel.addDishToCart() }
                .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
    }

}