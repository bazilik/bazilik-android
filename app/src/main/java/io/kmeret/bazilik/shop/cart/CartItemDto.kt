package io.kmeret.bazilik.shop.cart

import io.kmeret.common.lists.Identifiable

data class CartItemDto(
        override val id: Long = 0,
        var count: Int = 0,
        val dishId: Long,
        val dishName: String,
        val dishPhotoUrl: String,
        val dishPrice: Float,
        val shopLogoUrl: String
) : Identifiable {

    fun getAmount() = (dishPrice * count).toString()

    fun getTotalInfo() = "$dishPrice * $count = ${getAmount()}"

    fun changeCount(delta: Int) {
        val newCount = count + delta
        if (newCount < 1) return
        count = newCount
    }

}