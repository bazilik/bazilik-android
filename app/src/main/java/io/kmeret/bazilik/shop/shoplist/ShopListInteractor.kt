package io.kmeret.bazilik.shop.shoplist

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.storage.dao.ShopDao

class ShopListInteractor(private val tokenManager: TokenManager,
                         private val shopDao: ShopDao,
                         private val apiService: ApiService
) : BaseInteractor() {

    fun getShopList() = shopDao.findAll()

    fun refreshShopList() {
        apiService.getShopList(tokenManager.getApiToken()).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }

            shopDao.saveAll(it.map { it.map() })
        }
    }

}