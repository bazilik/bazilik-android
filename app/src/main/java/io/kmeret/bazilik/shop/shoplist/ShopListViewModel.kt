package io.kmeret.bazilik.shop.shoplist

import android.arch.lifecycle.LiveData
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.storage.entity.Shop

class ShopListViewModel(private val shopListInteractor: ShopListInteractor)
    : BaseViewModel(listOf(shopListInteractor)) {

    val shopList: LiveData<List<Shop>> = shopListInteractor.getShopList()

    fun refresh() = shopListInteractor.refreshShopList()

}