package io.kmeret.bazilik

import android.annotation.SuppressLint
import android.app.Application
import com.vk.sdk.VKSdk
import io.kmeret.bazilik.api.ApiModule
import io.kmeret.bazilik.auth.AuthModule
import io.kmeret.bazilik.contacts.ContactsModule
import io.kmeret.bazilik.feedback.FeedbackModule
import io.kmeret.bazilik.friends.FriendListModule
import io.kmeret.bazilik.message.MessageModule
import io.kmeret.bazilik.navigation.NavigationModule
import io.kmeret.bazilik.orders.OrderListModule
import io.kmeret.bazilik.profile.ProfileModule
import io.kmeret.bazilik.share.ShareModule
import io.kmeret.bazilik.shop.cart.CartModule
import io.kmeret.bazilik.shop.cart.add.AddCartModule
import io.kmeret.bazilik.shop.dish.DishModule
import io.kmeret.bazilik.shop.dishlist.DishListModule
import io.kmeret.bazilik.shop.shop.ShopModule
import io.kmeret.bazilik.shop.shoplist.ShopListModule
import io.kmeret.bazilik.stocks.StockModule
import io.kmeret.bazilik.storage.StorageModule
import org.koin.android.ext.android.startKoin

@SuppressLint("Registered")
open class App : Application() {

    protected var apiUrl = "https://api.buga.ga/"

    override fun onCreate() {
        super.onCreate()

        VKSdk.initialize(this)

        val moduleList = listOf(
                ApiModule,
                AuthModule,
                StorageModule,
                NavigationModule,
                ShopListModule,
                ShopModule,
                DishListModule,
                AddCartModule,
                CartModule,
                ProfileModule,
                OrderListModule,
                FeedbackModule,
                StockModule,
                ContactsModule,
                FriendListModule,
                ShareModule,
                MessageModule,
                DishModule
        )

        val propertiesMap = mapOf(
                "apiUrl" to apiUrl
        )

        startKoin(this, moduleList, propertiesMap)
    }

}
