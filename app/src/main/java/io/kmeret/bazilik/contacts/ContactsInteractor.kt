package io.kmeret.bazilik.contacts

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiContactsDto
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.ContactsNotImportedException
import io.kmeret.bazilik.frame.UserNotAuthException
import io.kmeret.bazilik.storage.dao.AuthUserDao

class ContactsInteractor(private val authUserDao: AuthUserDao,
                         private val tokenManager: TokenManager,
                         private val apiService: ApiService) : BaseInteractor() {

    var onContactListImported: (() -> Unit)? = null

    fun importContactList(phoneList: List<String>) {
        val user = authUserDao.getCurrent()
        if (user == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        val contactsDto = ApiContactsDto(user.id, phoneList)
        apiService.importContacts(tokenManager.getAccessToken(), contactsDto).request {
            if (!it.success) {
                onErrorCallback?.invoke(ContactsNotImportedException())
                return@request
            }

            onContactListImported?.invoke()
        }
    }

}