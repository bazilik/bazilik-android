@file:Suppress("DEPRECATION")

package io.kmeret.bazilik.contacts

import android.app.Activity
import android.app.LoaderManager
import android.content.Context
import android.content.CursorLoader
import android.content.Loader
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import io.kmeret.bazilik.storage.dao.ContactDao
import io.kmeret.bazilik.storage.entity.Contact
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import java.util.*

class ContactsProvider(private val context: Context,
                       private val contactDao: ContactDao)
    : LoaderManager.LoaderCallbacks<Cursor> {

    companion object {
        private const val LOADER_ID = 1
    }

    var onPhoneListReceived: ((List<String>) -> Unit)? = null

    fun requestContactList(activity: Activity) {
        if (activity.loaderManager.getLoader<Any>(LOADER_ID) == null)
            activity.loaderManager.initLoader(LOADER_ID, null, this)
        else
            activity.loaderManager.restartLoader(LOADER_ID, null, this)
    }

    override fun onCreateLoader(id: Int, args: Bundle?) = CursorLoader(
            context,
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
    )

    override fun onLoadFinished(loader: Loader<Cursor>, contactsCursor: Cursor) {

        val contactList = ArrayList<String>()

        contactDao.deleteAll()

        val phoneUtil = PhoneNumberUtil.createInstance(context)

        while (contactsCursor.moveToNext()) {

            val name = contactsCursor.getString(
                    contactsCursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY))

            val rawNumber = contactsCursor.getString(
                    contactsCursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER))

            if (!rawNumber.startsWith("+7") && !rawNumber.startsWith("8")) continue

            val parsedNumber = phoneUtil.parse(rawNumber, Locale.getDefault().country)
            val formattedNumber = phoneUtil.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164)

            contactList.add(formattedNumber)

            contactDao.insert(Contact(contactList.size + 1L, name, formattedNumber))
        }

        contactsCursor.close()

        onPhoneListReceived?.invoke(contactList)
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {}

}