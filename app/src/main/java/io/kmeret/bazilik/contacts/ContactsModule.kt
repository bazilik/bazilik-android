package io.kmeret.bazilik.contacts

import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object ContactsModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { ContactsProvider(androidApplication(), get()) }
            bean { ContactsInteractor(get(), get(), get()) }
            viewModel { ContactsViewModel(get(), get()) }
        }.invoke()
    }
}