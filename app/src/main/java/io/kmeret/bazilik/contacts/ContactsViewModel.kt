package io.kmeret.bazilik.contacts

import android.app.Activity
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.frame.SingleLiveEvent

class ContactsViewModel(private val contactsInteractor: ContactsInteractor,
                        private val contactsProvider: ContactsProvider
) : BaseViewModel(listOf(contactsInteractor)) {

    var contactImportedEvent = SingleLiveEvent<Boolean>()

    init {
        contactsInteractor.let {
            it.onContactListImported = { contactImportedEvent.value = true }
        }
        contactsProvider.let {
            it.onPhoneListReceived = { contactList -> contactsInteractor.importContactList(contactList) }
        }
    }

    fun importContacts(activity: Activity) = contactsProvider.requestContactList(activity)

}