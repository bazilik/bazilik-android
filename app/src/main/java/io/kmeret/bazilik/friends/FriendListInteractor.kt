package io.kmeret.bazilik.friends

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.storage.dao.AuthUserDao
import io.kmeret.bazilik.storage.dao.ContactDao
import io.kmeret.bazilik.storage.dao.FriendDao

class FriendListInteractor(private val authUserDao: AuthUserDao,
                           private val friendDao: FriendDao,
                           private val contactDao: ContactDao,
                           private val apiService: ApiService,
                           private val tokenManager: TokenManager
) : BaseInteractor() {

    fun getFriendList() = friendDao.getFriendList()

    fun getFilteredContactList() = contactDao.getAllWithoutFriends()

    fun requestFriendList() {
        val user = authUserDao.getCurrent() ?: return

        apiService.getFriendList(tokenManager.getAccessToken(), user.id).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }
            friendDao.saveAll(it.map { it.mapFriend() })
        }
    }


}