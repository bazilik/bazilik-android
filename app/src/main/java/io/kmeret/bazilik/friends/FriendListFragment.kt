package io.kmeret.bazilik.friends

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.storage.entity.User
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_friend_list.*
import kotlinx.android.synthetic.main.template_friend.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class FriendListFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_friend_list
    private val viewModel by viewModel<FriendListViewModel>()

    private lateinit var friendListAdapter: RecyclerAdapter<User>
//    private lateinit var contactListAdapter: RecyclerAdapter<Contact>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.friends))

        initFriendList()
//        initContactList()

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.friendList.subscribe(this) { friendListAdapter.updateList(it) }
//            it.contactList.subscribe(this) { contactListAdapter.updateList(it) }
            it.requestFriendList()
        }
    }

    private fun initFriendList() {
        friendListAdapter = RecyclerAdapter(R.layout.template_friend) { item, rootView ->
            with(rootView) {
                userAvatarView.loadWithUrl(item.avatarUrl)
                userNameView.text = item.name
                userPhoneView.text = item.phone
            }
        }
        friendListView.apply {
            attachAdapter(friendListAdapter)
            isNestedScrollingEnabled = false
        }
    }

//    private fun initContactList() {
//        contactListAdapter = RecyclerAdapter(R.layout.template_contact_list) { item, rootView ->
//            with(rootView) {
//                contactNameView.text = item.name
//                contactPhoneView.text = item.phone
//            }
//        }
//        contactListView.apply {
//            attachAdapter(contactListAdapter)
//            isNestedScrollingEnabled = false
//        }
//    }

}