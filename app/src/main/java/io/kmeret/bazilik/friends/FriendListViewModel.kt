package io.kmeret.bazilik.friends

import io.kmeret.bazilik.frame.BaseViewModel

class FriendListViewModel(private val friendListInteractor: FriendListInteractor)
    : BaseViewModel(listOf(friendListInteractor)) {

    val friendList = friendListInteractor.getFriendList()
//    val contactList = friendListInteractor.getFilteredContactList() //todo make paging

    fun requestFriendList() = friendListInteractor.requestFriendList()

}