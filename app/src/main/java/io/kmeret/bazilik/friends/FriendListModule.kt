package io.kmeret.bazilik.friends

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object FriendListModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { FriendListInteractor(get(), get(), get(), get(), get()) }
            viewModel { FriendListViewModel(get()) }
        }.invoke()
    }
}