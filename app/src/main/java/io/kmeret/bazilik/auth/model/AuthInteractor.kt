package io.kmeret.bazilik.auth.model

import android.app.Activity
import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.FirebaseAuthFailedException
import io.kmeret.bazilik.storage.dao.AuthUserDao

class AuthInteractor(private val firebaseAuthProvider: FirebaseAuthProvider,
                     private val tokenManager: TokenManager,
                     private val apiService: ApiService,
                     private val authUserDao: AuthUserDao
) : BaseInteractor() {

    var onSmsCodeReceived: ((smsCode: String) -> Unit)? = null
    var onApiTokenRetrieved: (() -> Unit)? = null
    var onFirebaseTokenRetrieved: (() -> Unit)? = null
    var onFirebaseAuthCompleted: (() -> Unit)? = null

    init {
        tokenManager.createKeystore()
        firebaseAuthProvider.run {
            onCodeSent = {}
            onCodeReceived = { code -> onSmsCodeReceived?.invoke(code) }
            onAuthFailed = { onErrorCallback?.invoke(FirebaseAuthFailedException()) }
            onAuthCompleted = { onFirebaseAuthCompleted?.invoke() }
            onTokenRetrieved = { token ->
                saveAccessToken(token)
                onFirebaseTokenRetrieved?.invoke()
            }
        }
    }

    fun requestSmsCode(context: Activity, phone: String) {
        firebaseAuthProvider.requestSmsCode(context, phone)
    }

    fun checkSmsCode(smsCode: String) {
        firebaseAuthProvider.signInBySmsCode(smsCode)
    }

    fun signOut() {
        firebaseAuthProvider.signOut()
        tokenManager.resetKeystore()
    }

    fun retrieveTokens() {
        apiService.getApiToken().request {
            tokenManager.saveApiToken(it.token)
            onApiTokenRetrieved?.invoke()
        }
        if (authUserDao.getCurrent() != null) firebaseAuthProvider.requestToken()
    }

    private fun saveAccessToken(token: String) {
        tokenManager.saveAccessToken(token)
    }

}