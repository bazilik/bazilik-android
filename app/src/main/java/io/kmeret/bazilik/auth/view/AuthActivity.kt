package io.kmeret.bazilik.auth.view

import android.os.Bundle
import io.kmeret.bazilik.R
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.navigation.NavigationActivity
import io.kmeret.bazilik.navigation.NavigationActivity.Companion.SIGN_IN
import io.kmeret.common.android.BaseActivity
import io.kmeret.common.android.onClick
import kotlinx.android.synthetic.main.layout_auth.*
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.koin.android.architecture.ext.viewModel

class AuthActivity : BaseActivity() {

    private val sharedViewModel by viewModel<AuthViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        actionBarVisible(false)
        setContentView(R.layout.layout_auth)
        fragmentLayoutResId = R.id.fragmentLayout
        closeButton.onClick { finish() }

        sharedViewModel.let {
            it.firebaseAuthCompletedEvent.subscribe(this) {
                if (it) restartNavigationActivity()
            }
            it.currentFragment.subscribe(this) {
                changeFragment(it)
            }
        }

        changeFragment(ChooseAuthMethodFragment(), false)
    }

    private fun restartNavigationActivity() {
        finish()
        startActivity(intentFor<NavigationActivity>(SIGN_IN to true).clearTop())
    }

}
