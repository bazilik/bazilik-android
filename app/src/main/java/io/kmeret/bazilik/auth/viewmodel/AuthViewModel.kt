package io.kmeret.bazilik.auth.viewmodel

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.support.v4.app.Fragment
import io.kmeret.bazilik.auth.model.AuthInteractor
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.auth.view.CreateProfileFragment
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.frame.SingleLiveEvent

class AuthViewModel(private val authInteractor: AuthInteractor,
                    private val userInteractor: UserInteractor)
    : BaseViewModel(listOf(authInteractor, userInteractor)) {

    val phoneNumber = MutableLiveData<String>()
    val smsCode = MutableLiveData<String>()
    val currentFragment = MutableLiveData<Fragment>()

    val smsCodeReceivedEvent = SingleLiveEvent<String>()
    val apiTokenRetrievedEvent = SingleLiveEvent<Boolean>()
    val firebaseTokenRetrievedEvent = SingleLiveEvent<Boolean>()
    val firebaseAuthCompletedEvent = SingleLiveEvent<Boolean>()

    init {
        authInteractor.apply {
            onSmsCodeReceived = { smsCodeReceivedEvent.value = it }
            onApiTokenRetrieved = { apiTokenRetrievedEvent.value = true }
            onFirebaseTokenRetrieved = { firebaseTokenRetrievedEvent.value = true }
            onFirebaseAuthCompleted = { checkUserIsExists() }
        }
        userInteractor.apply {
            onAuthFailed = { currentFragment.value = CreateProfileFragment() }
            onAuthCompleted = { firebaseAuthCompletedEvent.value = true }
        }
    }

    fun retrieveTokens() = authInteractor.retrieveTokens()

    fun requestSmsCode(context: Activity?) {
        if (context == null) return
        authInteractor.requestSmsCode(context, getPhoneNumber())
    }

    fun checkSmsCode() {
        val currentSmsCode = smsCode.value ?: return
        authInteractor.checkSmsCode(currentSmsCode)
    }

    fun createProfile(username: String, avatar: ByteArray) {
        if (username.isEmpty() || avatar.isEmpty()) return
        userInteractor.registerUser(username.trim(), getPhoneNumber(), avatar)
    }

    fun signOut() {
        authInteractor.signOut()
        userInteractor.deleteUserData()
    }

    private fun checkUserIsExists() {
        userInteractor.isUserExists(getPhoneNumber())
    }

    private fun getPhoneNumber(): String {
        val currentPhoneNumber = phoneNumber.value ?: throw NullPointerException()
        return "+7$currentPhoneNumber"
    }

}