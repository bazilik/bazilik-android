package io.kmeret.bazilik.auth

import io.kmeret.bazilik.auth.model.AuthInteractor
import io.kmeret.bazilik.auth.model.FirebaseAuthProvider
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object AuthModule : Module {
    override fun invoke() = applicationContext {
        bean { FirebaseAuthProvider() }
        bean { TokenManager(get()) }
        bean { AuthInteractor(get(), get(), get(), get()) }
        bean { UserInteractor(get(), get(), get(), get(), get(), get(), get(), get(), get()) }
        viewModel { AuthViewModel(get(), get()) }
    }.invoke()
}