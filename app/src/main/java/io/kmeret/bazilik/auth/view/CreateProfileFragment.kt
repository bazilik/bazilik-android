package io.kmeret.bazilik.auth.view

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.onClick
import kotlinx.android.synthetic.main.layout_create_profile.*
import org.koin.android.architecture.ext.sharedViewModel
import java.io.ByteArrayOutputStream
import java.io.File

class CreateProfileFragment : BaseFragment() {

    companion object {
        private const val CAMERA_CODE = 1
        private const val GALLERY_CODE = 2
    }

    override val layoutResId = R.layout.layout_create_profile
    private val viewModel by sharedViewModel<AuthViewModel>()
    private var uri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraButton.onClick { openCameraActivity() }
        galleryButton.onClick { openGalleryActivity() }
        saveButton.onClick { viewModel.createProfile(userNameView.text.toString(), getAvatar()) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            super.onActivityResult(requestCode, Activity.RESULT_CANCELED, data)
            return
        }

        val uri = when (requestCode) {
            CAMERA_CODE -> uri
            GALLERY_CODE -> data?.data
            else -> return
        }

        val bitmap =  MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
        avatarView.setBitmap(bitmap)
    }

    private fun openCameraActivity() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(requireActivity().packageManager) != null) {
            val folder = File(requireContext().filesDir.absolutePath + "/temp").also {
                it.delete()
                it.mkdirs()
            }
            uri = FileProvider.getUriForFile(
                    requireContext(),
                    "io.kmeret.bazilik.fileprovider",
                    File.createTempFile("avatar", ".jpg", folder)
            )
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            startActivityForResult(takePictureIntent, CAMERA_CODE)
        }
    }

    private fun openGalleryActivity() {
        val intent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        startActivityForResult(intent, GALLERY_CODE)
    }

    private fun getAvatar(): ByteArray {
        val outputStream = ByteArrayOutputStream()
        avatarView.getCroppedBitmap()?.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
        return outputStream.toByteArray()
    }

}