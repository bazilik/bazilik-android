package io.kmeret.bazilik.auth.view

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.keyboardVisible
import io.kmeret.common.android.onClick
import io.kmeret.common.android.onTextChanged
import kotlinx.android.synthetic.main.layout_choose_auth_method.*
import org.koin.android.architecture.ext.sharedViewModel

class ChooseAuthMethodFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_choose_auth_method
    private val viewModel by sharedViewModel<AuthViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signInButton.onClick {
            val currentPhoneString = userPhoneView.text
            if (currentPhoneString != null && currentPhoneString.length < 10) {
                userPhoneView.error = getString(R.string.incorrectPhone)
                return@onClick
            }
            viewModel.currentFragment.value = SmsCodeFragment()
        }

        userPhoneView.onTextChanged { text, length ->
            if (length == 10) userPhoneView.keyboardVisible(false)
            viewModel.phoneNumber.value = text
        }
    }

}