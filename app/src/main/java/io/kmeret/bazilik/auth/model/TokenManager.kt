package io.kmeret.bazilik.auth.model

import io.kmeret.bazilik.storage.dao.KeystoreDao
import io.kmeret.bazilik.storage.entity.Keystore

class TokenManager(private val keystoreDao: KeystoreDao) {

   fun getApiToken(): String {
       //        if (apiToken.isEmpty()) throw Exception("Null api token!")
        return getKeystore().apiToken
    }

    fun getAccessToken(): String {
        //        if (accessToken.isEmpty()) throw Exception("Null access token!")
        return getKeystore().accessToken
    }

    fun createKeystore() {
        if (keystoreDao.getKeyStore() != null) return
        keystoreDao.createKeyStore(Keystore.create())
    }

    fun resetKeystore() = keystoreDao.resetKeyStore(Keystore.create())

    fun saveApiToken(token: String) = keystoreDao.saveApiToken(token)

    fun saveAccessToken(token: String) = keystoreDao.saveAccessToken(token)

    private fun getKeystore() =
            keystoreDao.getKeyStore() ?: throw Exception("Null keystore!")

}