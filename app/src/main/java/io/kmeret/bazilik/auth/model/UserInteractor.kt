package io.kmeret.bazilik.auth.model

import com.google.gson.Gson
import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.storage.dao.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

class UserInteractor(private val apiService: ApiService,
                     private val authUserDao: AuthUserDao,
                     private val tokenManager: TokenManager,
                     private val deliveredDishDao: DeliveredDishDao,
                     private val cartItemDao: CartItemDao,
                     private val addressDao: AddressDao,
                     private val friendDao: FriendDao,
                     private val contactDao: ContactDao,
                     private val messageDao: MessageDao
) : BaseInteractor() {

    data class RegisterUser(val fullName: String, val phone: String)

    var onAuthCompleted: (() -> Unit)? = null
    var onAuthFailed: (() -> Unit)? = null

    fun getAuthorized() = authUserDao.getLiveCurrent()

    fun isUserExists(phone: String) {
        apiService.checkPhone(tokenManager.getApiToken(), phone).request {
            if (it.exists) authUserDao.insert(it.user!!.mapAuthUser())
            when (it.exists) {
                false -> onAuthFailed?.invoke()
                true -> onAuthCompleted?.invoke()
            }
        }
    }

    fun registerUser(name: String, phoneNumber: String, avatar: ByteArray) {

        val json = Gson().toJson(RegisterUser(name, phoneNumber))

        val userRequestBody = RequestBody.create(MultipartBody.FORM, json)
        val avatarRequestBody = RequestBody.create(MediaType.parse("image/jpeg"), avatar)
        val avatarPart = MultipartBody.Part
                .createFormData("avatar", "avatar.jpeg", avatarRequestBody)

        apiService.register(tokenManager.getApiToken(), userRequestBody, avatarPart).request {
            authUserDao.insert(it.mapAuthUser())
            onAuthCompleted?.invoke()
        }
    }

    fun deleteUserData() {
        addressDao.deleteAll()
        authUserDao.deleteCurrent()
        cartItemDao.deleteAll()
        contactDao.deleteAll()
        deliveredDishDao.deleteAll()
        friendDao.deleteAll()
        messageDao.deleteAll()
    }

}