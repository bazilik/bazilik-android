package io.kmeret.bazilik.auth.model

import android.app.Activity
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class FirebaseAuthProvider {

    private var currentVerificationId = ""
    private var currentResendToken: PhoneAuthProvider.ForceResendingToken? = null
    private val smsTimeoutSeconds = 60L

    var onCodeSent: (() -> Unit)? = null
    var onCodeReceived: ((code: String) -> Unit)? = null
    var onAuthFailed: ((Exception) -> Unit)? = null
    var onAuthCompleted: (() -> Unit)? = null
    var onTokenRetrieved: ((token: String) -> Unit)? = null

    private val verificationListener =
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onCodeSent(verificationId: String,
                                        token: PhoneAuthProvider.ForceResendingToken) {
                    currentVerificationId = verificationId
                    currentResendToken = token
                    this@FirebaseAuthProvider.onCodeSent?.invoke()
                }

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    signIn(credential)
                }

                override fun onCodeAutoRetrievalTimeOut(verificationId: String) {
                    onAuthFailed?.invoke(Exception())
                }

                override fun onVerificationFailed(ex: FirebaseException) {
                    onAuthFailed?.invoke(ex)
                }

            }

    fun requestSmsCode(context: Activity,
                       phone: String,
                       token: PhoneAuthProvider.ForceResendingToken? = null) {
        when (token == null) {
            true -> PhoneAuthProvider.getInstance()
                    .verifyPhoneNumber(phone, smsTimeoutSeconds, TimeUnit.SECONDS, context, verificationListener)
            false -> PhoneAuthProvider.getInstance()
                    .verifyPhoneNumber(phone, smsTimeoutSeconds, TimeUnit.SECONDS, context, verificationListener, token)
        }
    }

    fun signInBySmsCode(smsCode: String) {
        val credential = PhoneAuthProvider.getCredential(currentVerificationId, smsCode)
        signIn(credential)
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
    }

    fun requestToken() {
        val user = FirebaseAuth.getInstance().currentUser ?: return
        user.getIdToken(true).addOnCompleteListener {task ->
            if (!task.isSuccessful) {
                onAuthFailed?.invoke(Exception(task.exception))
                return@addOnCompleteListener
            }

            val token = task.result?.token
            if (token == null) {
                onAuthFailed?.invoke(Exception("Null auth token!"))
                return@addOnCompleteListener
            }

            onTokenRetrieved?.invoke(token)
        }
    }

    private fun signIn(credential: PhoneAuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(credential)
                .addOnCompleteListener {
                    val smsCode = credential.smsCode
                    if (smsCode != null) onCodeReceived?.invoke(smsCode)
                    onAuthCompleted?.invoke()
                    requestToken()
                }
    }

}