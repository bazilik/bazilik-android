package io.kmeret.bazilik.auth.view

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.auth.viewmodel.AuthViewModel
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.onTextChanged
import kotlinx.android.synthetic.main.layout_sms_code.*
import org.koin.android.architecture.ext.sharedViewModel

class SmsCodeFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_sms_code
    private val viewModel by sharedViewModel<AuthViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.let {
            it.smsCodeReceivedEvent.subscribe(this) {
                smsCodeView.setText(it)
                viewModel.smsCode.value = it
            }
        }

        smsCodeView.onTextChanged { text, length ->
            if (length == 6) viewModel.checkSmsCode()
            viewModel.smsCode.value = text
        }

        viewModel.requestSmsCode(activity as AuthActivity)
    }

}
