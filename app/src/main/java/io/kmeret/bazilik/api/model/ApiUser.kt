package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.AuthUser
import io.kmeret.bazilik.storage.entity.Friend
import io.kmeret.bazilik.storage.entity.User

data class ApiUser(
        val id: Long,
        val fullName: String,
        val phone: String,
        val avatarUrl: String,
        val notificationEnabled: Boolean = false
) {
    fun mapAuthUser() = AuthUser(id, fullName, phone, avatarUrl, notificationEnabled)
    fun mapUser() = User(id, fullName, phone, avatarUrl)
    fun mapFriend() = Friend(id, fullName, phone, avatarUrl)
}
