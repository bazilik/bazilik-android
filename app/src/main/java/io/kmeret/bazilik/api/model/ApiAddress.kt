package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Address

data class ApiAddress (val id: Long,
                       val text: String,
                       val user: ApiUser) {
    fun map() = Address(id, text, user.id)
}