package io.kmeret.bazilik.api

import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import io.kmeret.bazilik.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiModule : Module {
    override fun invoke() = applicationContext {
        bean {

            val retrofitBuilder = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                    .baseUrl(getProperty<String>("apiUrl"))

            val clientBuilder = OkHttpClient.Builder()

            if (BuildConfig.DEBUG) {
                clientBuilder.addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                Picasso.get().apply {
                    isLoggingEnabled = true
                    //setIndicatorsEnabled(true)
                }
            }

            val client = clientBuilder.build()
            val retrofit = retrofitBuilder.client(client).build()

            retrofit.create(ApiService::class.java)
        }
    }.invoke()
}
