package io.kmeret.bazilik.api.model

data class ApiResponse(val success: Boolean)