package io.kmeret.bazilik.api.model

data class ApiNewComment(
        val text: String,
        val userId: Long,
        val dishId: Long
)