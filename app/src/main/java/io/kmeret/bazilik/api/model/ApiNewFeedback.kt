package io.kmeret.bazilik.api.model

data class ApiNewFeedback(val userId: Long, val text: String)