package io.kmeret.bazilik.api.model

data class ApiContactsDto(val userId: Long, val phoneList: List<String>)