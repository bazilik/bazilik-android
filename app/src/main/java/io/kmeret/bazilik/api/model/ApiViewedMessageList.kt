package io.kmeret.bazilik.api.model

data class ApiViewedMessageList(val idList: List<Long>)