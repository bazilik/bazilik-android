package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Message

data class ApiMessage(
        val id: Long,
        val userFrom: ApiUser,
        val userTo: ApiUser,
        val dish: ApiDish,
        val comment: String,
        val sentAt: Long,
        val viewed: Boolean
) {
    fun map() = Message(id, userFrom.id, userTo.id, dish.id, comment, sentAt, viewed)
}