package io.kmeret.bazilik.api.model

class ApiDeliveredDish(val dish: ApiDish,
                       val count: Int,
                       val createdAt: Long)