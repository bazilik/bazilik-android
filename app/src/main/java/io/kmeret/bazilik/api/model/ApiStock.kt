package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Stock

data class ApiStock(
        val id: Long,
        val description: String,
        val expiredAt: Long,
        var photoUrl: String,
        var shop: ApiShop
) {
    fun map() = Stock(id, description, expiredAt, photoUrl, shop.id)
}