package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Dish

data class ApiDish(
        val id: Long,
        val name: String,
        val description: String,
        val price: Float,
        val photoUrl: String,
        val commentCount: Int,
        val category: ApiCategory
) {
    fun map() = Dish(id, name, description, price, photoUrl, commentCount, category.id)
}