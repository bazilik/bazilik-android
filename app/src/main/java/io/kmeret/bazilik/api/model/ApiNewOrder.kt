package io.kmeret.bazilik.api.model

data class ApiNewOrder(val addressId: Long,
                       val comment: String,
                       val cartItemList: List<ApiNewCartItem>)