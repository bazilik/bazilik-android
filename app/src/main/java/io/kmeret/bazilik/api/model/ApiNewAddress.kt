package io.kmeret.bazilik.api.model

data class ApiNewAddress(val text: String, val userId: Long)