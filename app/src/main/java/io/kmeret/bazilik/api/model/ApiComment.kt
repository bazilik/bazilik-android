package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Comment

class ApiComment(
        val id: Long,
        val text: String,
        val createdAt: Long,
        val user: ApiUser
) {
    fun map(dishId: Long) = Comment(id, text, createdAt, user.id, dishId)
}