package io.kmeret.bazilik.api.model

data class ApiNewCartItem(val dishId: Long, val count: Int)