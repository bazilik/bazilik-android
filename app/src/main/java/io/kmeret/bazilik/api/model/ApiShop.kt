package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Shop

data class ApiShop(
        val id: Long,
        val name: String,
        var logoUrl: String
) {
    fun map() = Shop(id, name, logoUrl)
}