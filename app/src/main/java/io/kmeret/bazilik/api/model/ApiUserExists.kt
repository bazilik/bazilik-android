package io.kmeret.bazilik.api.model

data class ApiUserExists(val exists: Boolean, val user: ApiUser?)