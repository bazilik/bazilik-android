package io.kmeret.bazilik.api.model

data class ApiToken(val token: String)