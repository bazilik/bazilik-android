package io.kmeret.bazilik.api

import io.kmeret.bazilik.api.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    /*AUTH*/

    @GET("auth/api")
    fun getApiToken(): Call<ApiToken>

    @GET("auth/check")
    fun checkPhone(@Header("Api-Token") apiToken: String,
                   @Query("phone") phone: String): Call<ApiUserExists>

    @Multipart
    @POST("auth/register")
    fun register(@Header("Api-Token") apiToken: String,
                 @Part("user") user: RequestBody,
                 @Part avatar: MultipartBody.Part): Call<ApiUser>

    /*ADDRESS*/

    @GET("address/all")
    fun getAddressList(@Header("Firebase-Token") accessToken: String,
                       @Query("userId") userId: Long): Call<List<ApiAddress>>

    @POST("address/create")
    fun createAddress(@Header("Firebase-Token") accessToken: String,
                      @Body newAddress: ApiNewAddress): Call<ApiAddress>

    @DELETE("address/delete")
    fun deleteAddress(@Header("Firebase-Token") accessToken: String,
                      @Query("addressId") addressId: Long): Call<ApiResponse>

    /*SHOP*/

    @GET("shop/all")
    fun getShopList(@Header("Api-Token") apiToken: String): Call<List<ApiShop>>

    @GET("category")
    fun getCategoryList(@Header("Api-Token") apiToken: String,
                        @Query("shopId") shopId: Long): Call<List<ApiCategory>>

    @GET("dish")
    fun getDishList(@Header("Api-Token") apiToken: String,
                    @Query("categoryId") categoryId: Long): Call<List<ApiDish>>

    @GET("dish/find")
    fun getDishListByIds(@Header("Api-Token") apiToken: String,
                         @Query("id") dishId: List<Long>): Call<List<ApiDish>>

    @GET("stock/all")
    fun getStockList(@Header("Api-Token") apiToken: String): Call<List<ApiStock>>

    /*CART*/

    @POST("order/create")
    fun sendOrder(@Header("Firebase-Token") accessToken: String,
                  @Body newOrder: ApiNewOrder): Call<ApiResponse>

    @GET("order/delivered")
    fun getDeliveredDishList(@Header("Firebase-Token") accessToken: String,
                             @Query("userId") userId: Long): Call<List<ApiDeliveredDish>>

    /*FEEDBACK*/

    @POST("feedback/create")
    fun sendFeedback(@Header("Firebase-Token") accessToken: String,
                     @Body newFeedback: ApiNewFeedback
    ): Call<ApiResponse>

    /*FRIENDS*/

    @POST("contact/import")
    fun importContacts(@Header("Firebase-Token") accessToken: String,
                       @Body apiContactsDto: ApiContactsDto): Call<ApiResponse>

    @GET("contact/friends")
    fun getFriendList(@Header("Firebase-Token") accessToken: String,
                      @Query("userId") userId: Long): Call<List<ApiUser>>

    /*MESSAGES*/

    @POST("message/send/dish")
    fun sendDishMessage(@Header("Firebase-Token") accessToken: String,
                        @Body apiNewDishMessage: ApiNewDishMessage): Call<ApiResponse>

    @GET("message/unviewed")
    fun getUnviewedMessageList(@Header("Firebase-Token") accessToken: String,
                               @Query("userId") userId: Long): Call<List<ApiMessage>>

    @POST("message/view")
    fun sendViewedMessageIdList(@Header("Firebase-Token") accessToken: String,
                                @Body apiViewedMessageList: ApiViewedMessageList): Call<ApiResponse>

    /*COMMENTS*/

    @POST("comment/add")
    fun sendComment(@Header("Firebase-Token") accessToken: String,
                    @Body apiNewComment: ApiNewComment): Call<ApiComment>

    @GET("comment/list")
    fun getCommentListByDishId(@Header("Api-Token") apiToken: String,
                               @Query("dishId") dishId: Long): Call<List<ApiComment>>


}