package io.kmeret.bazilik.api.model

import io.kmeret.bazilik.storage.entity.Category

data class ApiCategory(
        val id: Long,
        val name: String,
        var photoUrl: String,
        var shop: ApiShop
) {
    fun map() = Category(id, name, photoUrl, shop.id)
}