package io.kmeret.bazilik.api.model

data class ApiNewDishMessage(
        val userId: Long,
        val friendId: Long,
        val dishId: Long,
        val comment: String
)