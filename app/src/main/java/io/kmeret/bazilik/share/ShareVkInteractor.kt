package io.kmeret.bazilik.share

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import com.vk.sdk.api.photo.VKImageParameters
import com.vk.sdk.api.photo.VKUploadImage
import com.vk.sdk.dialogs.VKShareDialog
import com.vk.sdk.dialogs.VKShareDialogBuilder
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.VkException
import io.kmeret.bazilik.storage.dao.DishDao


class ShareVkInteractor(private val dishDao: DishDao) : BaseInteractor() {

    companion object {
        const val TAG = "VK_SHARE_DIALOG"
    }

    private var accessToken: VKAccessToken? = null
    private var dishId: Long = 0L
    private var comment: String = ""

    var onAuthCompleted: (() -> Unit)? = null
    var onDialogReady: ((VKShareDialogBuilder) -> Unit)? = null
    var onShareCompleted: (() -> Unit)? = null

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) =
            VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
                override fun onResult(res: VKAccessToken) {
                    accessToken = res
                    onAuthCompleted?.invoke()
                }

                override fun onError(error: VKError) {
                    Log.d("VK", error.toString())
                    onErrorCallback?.invoke(VkException())
                }
            })

    fun shareDish(dishId: Long, comment: String, activity: AppCompatActivity) {
        this.dishId = dishId
        this.comment = comment
        onAuthCompleted = { makeDialog() }
        onDialogReady = { it.show(activity.supportFragmentManager, TAG) }
        login(activity)
    }

    private fun makeDialog() {
        val dish = dishDao.getByIdWithShop(dishId)

        Picasso.get().load(dish.dishPhotoUrl).into(object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {

                val dialog = VKShareDialogBuilder().apply {
                    setShareDialogListener(object : VKShareDialog.VKShareDialogListener {
                        override fun onVkShareComplete(postId: Int) {
                            onShareCompleted?.invoke()
                        }

                        override fun onVkShareError(error: VKError) {
                            Log.d("VK", error.toString())
                            onErrorCallback?.invoke(VkException())
                        }

                        override fun onVkShareCancel() {}

                    })
                    setText("$comment \n Заказывайте! ${dish.dishName} в ${dish.shopName}!")
                    setAttachmentImages(arrayOf(VKUploadImage(bitmap, VKImageParameters.pngImage())))
                    setAttachmentLink("Скачай приложение Базилик из Play Market", "https://buga.ga")
                }

                onDialogReady?.invoke(dialog)
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        })
    }

    private fun login(activity: Activity) {
        if (VKSdk.isLoggedIn()) {
            onAuthCompleted?.invoke()
            return
        }
        VKSdk.login(activity, VKScope.WALL, VKScope.PHOTOS)
    }

}