package io.kmeret.bazilik.share

import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object ShareModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { ShareInteractor(get(), get(), get()) }
            bean { ShareVkInteractor(get()) }
            viewModel { ShareViewModel(get(), get()) }
        }.invoke()
    }
}