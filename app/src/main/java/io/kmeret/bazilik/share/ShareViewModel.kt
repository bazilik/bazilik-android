package io.kmeret.bazilik.share

import android.arch.lifecycle.MutableLiveData
import android.support.v7.app.AppCompatActivity
import com.vk.sdk.dialogs.VKShareDialogBuilder
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.frame.SingleLiveEvent

class ShareViewModel(private val shareInteractor: ShareInteractor,
                     private val shareVkInteractor: ShareVkInteractor
) : BaseViewModel(listOf(shareInteractor, shareVkInteractor)) {

    val pickedFriendId = MutableLiveData<Long>()
    val lastPickedFriendId = MutableLiveData<Long>()
    val messageSentEvent = SingleLiveEvent<Boolean>()
    val vkSelected = MutableLiveData<Boolean>()
    val vkShareDialog = MutableLiveData<VKShareDialogBuilder>()

    init {
        shareInteractor.onMessageSent = { messageSentEvent.value = true }
        shareVkInteractor.onShareCompleted = { messageSentEvent.value = true }
    }

    fun sendDish(dishId: Long, comment: String, context: AppCompatActivity) {
        val shareToVk = vkSelected.value
        if (shareToVk != null && shareToVk) {
            sendDishToVk(dishId, comment, context)
            return
        }

        val friendId = pickedFriendId.value ?: return
        shareInteractor.sendDishToFriend(dishId, comment, friendId)
    }

    private fun sendDishToVk(dishId: Long, comment: String, context: AppCompatActivity) {
        shareVkInteractor.shareDish(dishId, comment, context)
    }

}
