package io.kmeret.bazilik.share

import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.friends.FriendListViewModel
import io.kmeret.bazilik.storage.entity.User
import io.kmeret.common.android.onClick
import io.kmeret.common.android.onFocus
import io.kmeret.common.android.visibility
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_share.*
import kotlinx.android.synthetic.main.layout_share.view.*
import kotlinx.android.synthetic.main.template_friend_share.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class ShareDialog : BottomSheetDialogFragment() {

    companion object {
        const val TAG = "ShareDialog"
        const val DISH_ID = "DISH_ID"
        fun create(dishId: Long) = ShareDialog().apply {
            arguments = Bundle().apply {
                putLong(DISH_ID, dishId)
            }
        }
    }

    private lateinit var layout: View
    private lateinit var friendListAdapter: RecyclerAdapter<User>
    private val friendListViewModel by viewModel<FriendListViewModel>()
    private val shareViewModel by viewModel<ShareViewModel>()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        layout = inflater.inflate(R.layout.layout_share, container, false)

        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        initFriendListView()

        friendListViewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.friendList.subscribe(this) { friendListAdapter.updateList(it) }
            it.requestFriendList()
        }
        shareViewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.pickedFriendId.subscribe(this) { selectFriend(it) }
            it.messageSentEvent.subscribe(this) {
                if (it) {
                    requireContext().toast(R.string.messageSent)
                    this.dismiss()
                }
            }
            it.vkShareDialog.subscribe(this) { it.show(childFragmentManager, ShareVkInteractor.TAG) }
        }
        layout.apply {
            vkButton.onClick { selectVk() }
            commentEditTextView.onFocus { expandModal() }
            sendDishButton.onClick {
                val dishId = arguments?.getLong(DISH_ID) ?: return@onClick
                shareViewModel.sendDish(
                        dishId,
                        commentEditTextView.text.toString(),
                        requireActivity() as AppCompatActivity
                )
            }
        }

        return layout
    }

    private fun initFriendListView() {
        friendListAdapter = RecyclerAdapter(R.layout.template_friend_share) { item, rootView ->
            with(rootView) {
                userAvatarView.loadWithUrl(item.avatarUrl)
                userNameView.text = item.name
                userLayout.onClick { shareViewModel.pickedFriendId.value = item.id }
            }
        }
        layout.friendListView.apply {
            isNestedScrollingEnabled = false
            setHasFixedSize(false)
            layoutManager = LinearLayoutManager(requireContext(),
                    LinearLayout.HORIZONTAL,
                    false)
            adapter = friendListAdapter
        }
    }

    private fun selectFriend(userId: Long) {
        val previousUserId = shareViewModel.lastPickedFriendId.value
        if (previousUserId != null) changeFriendAvatarBorder(previousUserId, 0)
        val vkSelected = shareViewModel.vkSelected.value
        if (vkSelected != null && vkSelected) {
            vkButton.borderWidth = 0
            shareViewModel.vkSelected.value = false
        }

        shareViewModel.lastPickedFriendId.value = userId
        changeFriendAvatarBorder(userId, 3)
        showCommentViews()
    }

    private fun selectVk() {
        val previousUserId = shareViewModel.lastPickedFriendId.value
        if (previousUserId != null) {
            changeFriendAvatarBorder(previousUserId, 0)
            shareViewModel.lastPickedFriendId.value = null
        }
        shareViewModel.vkSelected.value = true
        vkButton.borderWidth = 3
        showCommentViews()
    }

    private fun changeFriendAvatarBorder(userId: Long, borderWidth: Int) {
        val rootView = (layout.friendListView.findViewHolderForItemId(userId)
                as RecyclerAdapter<*>.ItemViewHolder).rootView
        with(rootView) {
            userAvatarView.borderWidth = borderWidth
        }
    }

    private fun showCommentViews() {
        layout.apply {
            commentEditTextView.visibility(true)
            sendDishButton.visibility(true)
        }
    }

    private fun expandModal() {
        BottomSheetBehavior
                .from(dialog.findViewById(android.support.design.R.id.design_bottom_sheet)
                        as FrameLayout).state =
                BottomSheetBehavior.STATE_EXPANDED
    }

}