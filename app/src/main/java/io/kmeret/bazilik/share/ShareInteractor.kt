package io.kmeret.bazilik.share

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiNewDishMessage
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.MessageNotSentException
import io.kmeret.bazilik.frame.UserNotAuthException
import io.kmeret.bazilik.storage.dao.AuthUserDao

class ShareInteractor(private val authUserDao: AuthUserDao,
                      private val apiService: ApiService,
                      private val tokenManager: TokenManager) : BaseInteractor() {

    var onMessageSent: (() -> Unit)? = null

    fun sendDishToFriend(dishId: Long, comment: String, friendId: Long) {
        val authUser = authUserDao.getCurrent()
        if (authUser == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        val dishMessage = ApiNewDishMessage(authUser.id, friendId, dishId, comment)
        apiService.sendDishMessage(tokenManager.getAccessToken(), dishMessage).request {
            if (!it.success) {
                onErrorCallback?.invoke(MessageNotSentException())
                return@request
            }

            onMessageSent?.invoke()
        }

    }


}