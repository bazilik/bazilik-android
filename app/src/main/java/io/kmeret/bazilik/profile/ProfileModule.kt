package io.kmeret.bazilik.profile

import io.kmeret.bazilik.profile.model.AddressInteractor
import io.kmeret.bazilik.profile.viewmodel.ProfileViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.context.Context
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

object ProfileModule : Module {
    override fun invoke(): Context {
        return applicationContext {
            bean { AddressInteractor(get(), get(), get(), get()) }
            viewModel { ProfileViewModel(get(), get()) }
        }.invoke()
    }
}