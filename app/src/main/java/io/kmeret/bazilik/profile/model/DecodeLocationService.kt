package io.kmeret.bazilik.profile.model

import android.app.IntentService
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import io.kmeret.bazilik.profile.viewmodel.AddressDto
import io.kmeret.bazilik.profile.viewmodel.LocationDto
import java.util.*

class DecodeLocationService : IntentService("DecodeLocationService") {

    companion object {
        const val LOCATION = "LOCATION"
        const val RECEIVER = "RECEIVER"
        const val SUCCESS_RESULT = 0
        const val ADDRESS = "ADDRESS"
        const val FAILURE_RESULT = 1
        const val ERROR = "ERROR"
    }

    private var receiver: ResultReceiver? = null

    override fun onHandleIntent(intent: Intent?) {
        intent ?: return

        receiver = intent.getParcelableExtra(RECEIVER)

        try {
            val geoCoder = Geocoder(this, Locale.getDefault())
            val location = intent.getParcelableExtra<LocationDto>(LOCATION)
            val address = geoCoder
                    .getFromLocation(location.latitude, location.longitude, 1).first()

            val city = address.locality ?: throw NullPointerException("Null city!")
            val street = address.thoroughfare ?: throw NullPointerException("Null street!")
            val house = address.subThoroughfare ?: throw NullPointerException("Null house!")

            responseSuccess(AddressDto(city, street, house))
        } catch (ex: Exception) {
            val error = ex.message ?: "null"
            responseFailure(error)
            Log.d("DecodeLocationService", error)
        }

    }

    private fun responseSuccess(addressDto: AddressDto) {
        val bundle = Bundle().apply { putParcelable(ADDRESS, addressDto) }
        receiver?.send(SUCCESS_RESULT, bundle)
    }

    private fun responseFailure(message: String) {
        val bundle = Bundle().apply { putString(ERROR, message) }
        receiver?.send(FAILURE_RESULT, bundle)
    }

}