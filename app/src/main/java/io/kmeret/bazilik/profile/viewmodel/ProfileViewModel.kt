package io.kmeret.bazilik.profile.viewmodel

import android.arch.lifecycle.LiveData
import io.kmeret.bazilik.auth.model.UserInteractor
import io.kmeret.bazilik.frame.BaseViewModel
import io.kmeret.bazilik.profile.model.AddressInteractor
import io.kmeret.bazilik.storage.entity.Address
import io.kmeret.bazilik.storage.entity.AuthUser

class ProfileViewModel(userInteractor: UserInteractor,
                       private val addressInteractor: AddressInteractor
) : BaseViewModel(listOf(userInteractor, addressInteractor)) {

    val currentUser: LiveData<AuthUser?> = userInteractor.getAuthorized()
    val addressList: LiveData<List<Address>> = addressInteractor.getAddressList()

    fun requestAddressList() = addressInteractor.requestAddressList()

    fun saveAddress(address: String) = addressInteractor.requestCreateAddress(address)

    fun deleteAddress(id: Long) = addressInteractor.requestDeleteAddress(id)

}