package io.kmeret.bazilik.profile.model

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.IntentSender
import android.util.Log
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import io.kmeret.bazilik.profile.viewmodel.LocationDto

class GoogleServicesLocationProvider(private val activity: Activity) : LifecycleObserver {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 0x1
    }

    private val locationClient = LocationServices.getFusedLocationProviderClient(activity)
    private val locationRequest = LocationRequest().apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }
    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
            stopClient()
            requestingLocationUpdates = false
            result ?: return
            val location = result.lastLocation
            val locationDto = LocationDto(location.latitude, location.longitude)
            onLocationChanged?.invoke(locationDto)
        }
    }

    private val settingsClient = LocationServices.getSettingsClient(activity)
    private val settingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()

    private var requestingLocationUpdates = false

    var onLocationChanged: ((LocationDto) -> Unit)? = null

    fun getLocation() = settingsClient.checkLocationSettings(settingsRequest)
            .addOnSuccessListener { getGoodLocation() }
            .addOnFailureListener {
                if (it !is ResolvableApiException) {
                    getBadLocation()
                    return@addOnFailureListener
                }
                try {

                    it.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }

    fun onActivityResult(requestCode: Int, resultCode: Int) {
        if (requestCode != REQUEST_CHECK_SETTINGS) return
        when (resultCode) {
            Activity.RESULT_OK -> getGoodLocation()
            Activity.RESULT_CANCELED -> getBadLocation()
            else -> getBadLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getGoodLocation() {
        requestingLocationUpdates = true
        locationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    @SuppressLint("MissingPermission")
    private fun getBadLocation() {
        locationClient.lastLocation
                .addOnSuccessListener { location ->
                    location ?: return@addOnSuccessListener
                    val locationDto = LocationDto(location.latitude, location.longitude)
                    onLocationChanged?.invoke(locationDto)
                }
                .addOnFailureListener {
                    Log.d("GServiceLocatProvider", it.message)
                }
    }

    private fun stopClient() = locationClient.removeLocationUpdates(locationCallback)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        if (requestingLocationUpdates) getGoodLocation()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() = stopClient()

}