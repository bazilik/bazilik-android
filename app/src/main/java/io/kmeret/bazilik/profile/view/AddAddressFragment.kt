package io.kmeret.bazilik.profile.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.profile.model.DecodeLocationService
import io.kmeret.bazilik.profile.model.GoogleServicesLocationProvider
import io.kmeret.bazilik.profile.model.OpenStreetMapProvider
import io.kmeret.bazilik.profile.viewmodel.AddressDto
import io.kmeret.bazilik.profile.viewmodel.LocationDto
import io.kmeret.bazilik.profile.viewmodel.ProfileViewModel
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.PermissionManager
import io.kmeret.common.android.onClick
import io.kmeret.common.android.toEditable
import kotlinx.android.synthetic.main.layout_address_add.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.sharedViewModel

class AddAddressFragment : BaseFragment() {

    override val layoutResId = R.layout.layout_address_add
    private val viewModel by sharedViewModel<ProfileViewModel>()

    private lateinit var openStreetMapProvider: OpenStreetMapProvider
    private lateinit var googleServicesLocationProvider: GoogleServicesLocationProvider
    private val resultReceiver = AddressResultReceiver()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.addAddress))

        viewModel.errorMessage.subscribe(this) { requireContext().toast(it) }

        getPermissionManager().run {
            onPermissionsChecked = {
                initMap()
                initLocation()
            }
            requestPermission(PermissionManager.WRITE,
                    PermissionManager.GOOD_LOCATION,
                    PermissionManager.BAD_LOCATION)
        }

        deleteAddressButton.onClick { addressTextView.text?.clear() ?: return@onClick }
        addAddressButton.onClick {
            viewModel.saveAddress(addressTextView.text.toString())
            closeFragment()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        googleServicesLocationProvider.onActivityResult(requestCode, resultCode)
    }

    private fun initMap() {
        openStreetMapProvider = OpenStreetMapProvider(mapView).apply {
            this.onMapClick = { decodeLocation(it) }
            showLocation(LocationDto(46.3485763, 48.0333755))
        }
        lifecycle.addObserver(openStreetMapProvider)
    }

    private fun decodeLocation(point: LocationDto) {
        with(requireActivity()) {
            startService(intentFor<DecodeLocationService>(
                    DecodeLocationService.LOCATION to point,
                    DecodeLocationService.RECEIVER to resultReceiver
            ))
        }
    }

    private fun initLocation() {
        googleServicesLocationProvider = GoogleServicesLocationProvider(requireActivity()).apply {
            onLocationChanged = { openStreetMapProvider.showLocation(it) }
            getLocation()
        }
        lifecycle.addObserver(googleServicesLocationProvider)
    }

    inner class AddressResultReceiver : ResultReceiver(Handler()) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            resultData ?: return
            when (resultCode) {
                DecodeLocationService.SUCCESS_RESULT -> {
                    val address = resultData.getParcelable<AddressDto>(DecodeLocationService.ADDRESS) ?: return
                    addressTextView.text = address.toString().toEditable()
                }
                DecodeLocationService.FAILURE_RESULT -> {
                    val error = resultData.getString(DecodeLocationService.ERROR) ?: return
                    requireActivity().toast(error)
                }
            }
        }
    }

}