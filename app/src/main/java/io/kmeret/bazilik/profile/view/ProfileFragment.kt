package io.kmeret.bazilik.profile.view

import android.os.Bundle
import android.view.View
import io.kmeret.bazilik.R
import io.kmeret.bazilik.api.loadWithUrl
import io.kmeret.bazilik.frame.subscribe
import io.kmeret.bazilik.profile.viewmodel.ProfileViewModel
import io.kmeret.bazilik.storage.entity.Address
import io.kmeret.bazilik.storage.entity.AuthUser
import io.kmeret.common.android.BaseFragment
import io.kmeret.common.android.attachAdapter
import io.kmeret.common.android.onClick
import io.kmeret.common.lists.RecyclerAdapter
import kotlinx.android.synthetic.main.layout_profile.*
import kotlinx.android.synthetic.main.template_address.view.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.sharedViewModel

class ProfileFragment : BaseFragment() {

    override val layoutResId: Int = R.layout.layout_profile
    private val viewModel by sharedViewModel<ProfileViewModel>()
    private lateinit var addressListAdapter: RecyclerAdapter<Address>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeTitle(getString(R.string.profile))
        initAddressListView()
        addAddressButton.onClick { changeFragment(AddAddressFragment()) }

        viewModel.let {
            it.errorMessage.subscribe(this) { requireContext().toast(it) }
            it.currentUser.subscribe(this) { initUser(it) }
            it.addressList.subscribe(this) { addressListAdapter.updateList(it) }
            it.requestAddressList()
        }
    }

    private fun initUser(user: AuthUser?) {
        if (user == null) return
        userAvatarView.loadWithUrl(user.avatarUrl)
        userNameView.text = user.name
        userPhoneView.text = user.phone
        //userNotificationsEnabledView.isChecked = user.isNotificationEnabled

    }

    private fun initAddressListView() {
        addressListAdapter = RecyclerAdapter(R.layout.template_address) { item, rootView ->
            with(rootView) {
                addressTextView.text = item.text
                deleteAddressButton.onClick { viewModel.deleteAddress(item.id) }
            }
        }
        addressListView.attachAdapter(addressListAdapter)
        addressListView.isNestedScrollingEnabled = false
    }

}