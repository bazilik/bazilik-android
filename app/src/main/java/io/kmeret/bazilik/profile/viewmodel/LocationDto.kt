package io.kmeret.bazilik.profile.viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationDto(val latitude: Double, val longitude: Double) : Parcelable