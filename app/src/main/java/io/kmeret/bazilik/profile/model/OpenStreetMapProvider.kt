package io.kmeret.bazilik.profile.model

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.graphics.drawable.Drawable
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat
import android.view.MotionEvent
import io.kmeret.bazilik.R
import io.kmeret.bazilik.profile.viewmodel.LocationDto
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

class OpenStreetMapProvider(private val map: MapView) : LifecycleObserver {

    private val clickMarker = getMarker(getIcon(R.drawable.ic_location))
    private var currentZoom: Double = 17.0

    var onMapClick: ((location: LocationDto) -> Unit)? = null

    init {
        val context = map.context.applicationContext
        Configuration.getInstance()
                .load(context, PreferenceManager.getDefaultSharedPreferences(context))

        map.apply {
            setTileSource(TileSourceFactory.MAPNIK)
            setMultiTouchControls(true)
            setBuiltInZoomControls(false)
            setOnTouchListener { _, event ->
                val center = map.mapCenter
                val point = GeoPoint(center.latitude, center.longitude)
                when (event.action) {
                    MotionEvent.ACTION_MOVE -> moveMarker(point)
                    MotionEvent.ACTION_UP -> onMapClick?.invoke(toLocation(point))
                }
                false
            }
        }
        zoomCamera(currentZoom)
    }

    fun showLocation(location: LocationDto) {
        val point = toPoint(location)
        moveMarker(point)
        moveCamera(point)
    }

    private fun moveMarker(point: GeoPoint) {
        clickMarker.position = point
        if (map.overlays.contains(clickMarker)) return
        map.overlays.add(clickMarker)
    }

    private fun moveCamera(point: GeoPoint) {
        map.controller.animateTo(point)
    }

    private fun zoomCamera(zoom: Double) {
        map.controller.setZoom(zoom)
        currentZoom = zoom
    }

    private fun getMarker(icon: Drawable) = Marker(map).apply {
        setIcon(icon)
        setOnMarkerClickListener { _, _ -> true }
    }

    private fun getIcon(iconResId: Int) = ContextCompat.getDrawable(map.context, iconResId)
            ?: throw NullPointerException()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() = map.onResume()

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() = map.onPause()

    private fun toPoint(location: LocationDto) = GeoPoint(location.latitude, location.longitude)

    private fun toLocation(point: GeoPoint) = LocationDto(point.latitude, point.longitude)

}