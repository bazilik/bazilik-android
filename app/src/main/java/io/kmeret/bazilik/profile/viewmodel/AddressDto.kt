package io.kmeret.bazilik.profile.viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressDto(var city: String,
                      var street: String,
                      var house: String,
                      var flat: Int? = null) : Parcelable {
    override fun toString() = "г. $city, $street, $house"
}