package io.kmeret.bazilik.profile.model

import io.kmeret.bazilik.api.ApiService
import io.kmeret.bazilik.api.model.ApiNewAddress
import io.kmeret.bazilik.auth.model.TokenManager
import io.kmeret.bazilik.frame.AddressNotDeletedException
import io.kmeret.bazilik.frame.BaseInteractor
import io.kmeret.bazilik.frame.EmptyListException
import io.kmeret.bazilik.frame.UserNotAuthException
import io.kmeret.bazilik.storage.dao.AddressDao
import io.kmeret.bazilik.storage.dao.AuthUserDao

class AddressInteractor(private val addressDao: AddressDao,
                        private val authUserDao: AuthUserDao,
                        private val tokenManager: TokenManager,
                        private val apiService: ApiService
) : BaseInteractor() {

    fun getAddressList() = addressDao.findAll()

    fun getAddressById(addressId: Long) = addressDao.findById(addressId)

    fun requestAddressList() {
        val user = authUserDao.getCurrent()
        if (user == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        apiService.getAddressList(tokenManager.getAccessToken(), user.id).request {
            if (it.isEmpty()) {
                onErrorCallback?.invoke(EmptyListException())
                return@request
            }

            addressDao.saveAll(it.map { it.map() })
        }
    }

    fun requestCreateAddress(addressText: String) {
        val user = authUserDao.getCurrent()
        if (user == null) {
            onErrorCallback?.invoke(UserNotAuthException())
            return
        }

        val address = ApiNewAddress(addressText, user.id)
        apiService.createAddress(tokenManager.getAccessToken(), address).request {
            addressDao.insert(it.map())
        }
    }

    fun requestDeleteAddress(addressId: Long) {
        apiService.deleteAddress(tokenManager.getAccessToken(), addressId).request {
            if (!it.success) {
                onErrorCallback?.invoke(AddressNotDeletedException())
                return@request
            }

            addressDao.deleteById(addressId)
        }
    }

}